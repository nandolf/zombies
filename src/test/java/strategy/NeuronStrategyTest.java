package strategy;

import data.Point2D;
import genetic.neuronalnetwork.NetworkZombies;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class NeuronStrategyTest {

    @Test
    public void testAngle_1() throws Exception {
        NeuronStrategy neuronStrategy = new NeuronStrategy(mockNetwork(1.0, 1.0));
        String output = neuronStrategy.getNextSysO(new Point2D(1000, 1000), null, null);
        assertThat(output, is("1000 2000"));
    }

    @Test
    public void testAngle_05() throws Exception {
        NeuronStrategy neuronStrategy = new NeuronStrategy(mockNetwork(1.0, 0.5));
        String output = neuronStrategy.getNextSysO(new Point2D(1000, 1000), null, null);
        assertThat(output, is("1000 0"));
    }

    @Test
    public void testLength_1() throws Exception {
        NeuronStrategy neuronStrategy = new NeuronStrategy(mockNetwork(1.0, 0.0));
        String output = neuronStrategy.getNextSysO(new Point2D(1000, 1000), null, null);
        assertThat(output, is("1000 2000"));
    }

    @Test
    public void testLength_0() throws Exception {
        NeuronStrategy neuronStrategy = new NeuronStrategy(mockNetwork(0.0, 0.0));
        String output = neuronStrategy.getNextSysO(new Point2D(1000, 1000), null, null);
        assertThat(output, is("1000 1000"));
    }

    @Test
    public void testLength_05() throws Exception {
        NeuronStrategy neuronStrategy = new NeuronStrategy(mockNetwork(0.5, 0.0));
        String output = neuronStrategy.getNextSysO(new Point2D(1000, 1000), null, null);
        assertThat(output, is("1000 1500"));
    }

    @Test
    public void testAngleAndLength() throws Exception {
        NeuronStrategy neuronStrategy = new NeuronStrategy(mockNetwork(0.5, -0.25));
        String output = neuronStrategy.getNextSysO(new Point2D(1000, 1000), null, null);
        assertThat(output, is("1500 1000"));
    }


    private NetworkZombies mockNetwork(double length, double radius) {
        NetworkZombies networkMock = Mockito.mock(NetworkZombies.class);
        when(networkMock.getLengthOutput()).thenReturn(length);
        when(networkMock.getAngleOutput()).thenReturn(radius);
        return networkMock;
    }
}
