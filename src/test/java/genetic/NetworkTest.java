package genetic;

import data.Point2D;
import genetic.neuronalnetwork.Network;
import genetic.neuronalnetwork.NetworkZombies;
import genetic.neuronalnetwork.Neuron;
import genetic.sensors.BlockSensor;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class NetworkTest {

    @Test
    public void testOneNeuron() throws Exception {
        BlockSensor humanSensor = activatedSensorAt(0, 0);
        Neuron neuron = new Neuron(1, Collections.singletonMap(humanSensor, 0.5), (x) -> x * x);
        NetworkZombies network = new NetworkZombies(Arrays.asList(humanSensor, humanSensor, humanSensor), neuron, neuron);
        assertThat(network.getAngleOutput(), is(0.5 * 0.5));
        assertThat(network.getLengthOutput(), is(0.5 * 0.5));
        assertThat(network.hasCycle(), is(false));
    }

    @Test
    public void test3NeuronLayer() throws Exception {
        BlockSensor humanSensor = activatedSensorAt(0, 0);
        Neuron neuronStart = new Neuron(1, Collections.singletonMap(humanSensor, 0.5), (x) -> x);
        Neuron neuronMiddle = new Neuron(2, Collections.singletonMap(neuronStart, -1.0), (x) -> x);
        Neuron neuronEnd = new Neuron(3, Collections.singletonMap(neuronMiddle, 0.5), (x) -> x);

        NetworkZombies network = new NetworkZombies(Arrays.asList(humanSensor, humanSensor, humanSensor), neuronEnd, neuronEnd);
        assertThat(network.getAngleOutput(), is(0.5 * -1.0 * 0.5));
        assertThat(network.getLengthOutput(), is(0.5 * -1.0 * 0.5));
        assertThat(network.hasCycle(), is(false));
    }

    @Test
    public void testMultipleOutputsDiamond() throws Exception {
        BlockSensor humanSensor = activatedSensorAt(0, 0);
        Neuron neuronMiddle = new Neuron(1, Collections.singletonMap(humanSensor, 1.0), (x) -> x);
        Neuron neuronMiddle2 = new Neuron(2, Collections.singletonMap(humanSensor, 0.5), (x) -> x);
        Neuron neuronEnd = new Neuron(3, (x) -> x);
        neuronEnd.addInput(neuronMiddle, 1.0);
        neuronEnd.addInput(neuronMiddle2, 0.5);

        NetworkZombies network = new NetworkZombies(Arrays.asList(humanSensor, humanSensor, humanSensor), neuronEnd, neuronEnd);
        assertThat(network.getAngleOutput(), is(1 * 1 + 0.5 * 0.5));
        assertThat(network.getLengthOutput(), is(1 * 1 + 0.5 * 0.5));
        assertThat(network.hasCycle(), is(false));
    }

    @Test
    public void testNoCycle() throws Exception {
        BlockSensor humanSensor = activatedSensorAt(0, 0);
        Neuron neuron = new Neuron(1, (x) -> x);
        neuron.addInput(humanSensor, 1);
        Neuron neuronEnd = new Neuron(2, (x) -> x);
        neuronEnd.addInput(neuron, 1);

        Network network = new NetworkZombies(Arrays.asList(humanSensor, humanSensor, humanSensor), neuronEnd, neuronEnd);
        assertThat(network.hasCycle(), is(false));
    }

    @Test
    public void testSelfCycle() throws Exception {
        BlockSensor humanSensor = activatedSensorAt(0, 0);
        Neuron neuronEnd = new Neuron(1, (x) -> x);
        neuronEnd.addInput(neuronEnd, -0.5);

        Network network = new NetworkZombies(Arrays.asList(humanSensor, humanSensor, humanSensor), neuronEnd, neuronEnd);
        assertThat(network.hasCycle(), is(true));
    }

    @Test
    public void testCycle() throws Exception {
        BlockSensor humanSensor = activatedSensorAt(0, 0);
        Neuron neuron = new Neuron(1, (x) -> x);
        neuron.addInput(humanSensor, 1);
        Neuron neuronEnd = new Neuron(2, (x) -> x);
        neuronEnd.addInput(neuron, 1);

        neuron.addInput(neuronEnd, 1);

        Network network = new NetworkZombies(Arrays.asList(humanSensor, humanSensor, humanSensor), neuronEnd, neuronEnd);
        assertThat(network.hasCycle(), is(true));
    }

    @Test
    public void testCycleOnlyAngle() throws Exception {
        BlockSensor humanSensor = activatedSensorAt(0, 0);
        Neuron neuron = new Neuron(1, (x) -> x);
        neuron.addInput(humanSensor, 1);
        Neuron neuronAngle = new Neuron(2, (x) -> x);
        neuronAngle.addInput(neuron, 1);
        neuron.addInput(neuronAngle, 1);

        Neuron neuronLength = new Neuron(2, (x) -> x);
        neuronLength.addInput(neuron, 1);

        Network network = new NetworkZombies(Arrays.asList(humanSensor, humanSensor, humanSensor), neuronLength, neuronAngle);
        assertThat(network.hasCycle(), is(true));
    }

    @Test
    public void testCycleOnlyLength() throws Exception {
        BlockSensor humanSensor = activatedSensorAt(0, 0);
        Neuron neuron = new Neuron(1, (x) -> x);
        neuron.addInput(humanSensor, 1);
        Neuron neuronLength = new Neuron(2, (x) -> x);
        neuronLength.addInput(neuron, 1);
        neuron.addInput(neuronLength, 1);

        Neuron neuronAngle = new Neuron(2, (x) -> x);
        neuronAngle.addInput(neuron, 1);

        Network network = new NetworkZombies(Arrays.asList(humanSensor, humanSensor, humanSensor), neuronLength, neuronAngle);
        assertThat(network.hasCycle(), is(true));
    }

    @Test
    public void testTree() throws Exception {
        // 1->2, 1-3
        Neuron n1 = new Neuron(1L);
        Neuron n2 = new Neuron(2L);
        Neuron n3 = new Neuron(3L);
        n1.addInput(n2, 1.0);
        n1.addInput(n3, 1.0);

        NetworkZombies network = new NetworkZombies(Collections.emptyList(), n1, n1);
        assertThat(network.hasCycle(), is(false));
    }

    private BlockSensor activatedSensorAt(int x, int y) {
        BlockSensor sensor = new BlockSensor(new Point2D(x, y), 1);
        sensor.setFigures(Collections.singletonList(new Point2D(x, y)));
        return sensor;
    }
}