package genetic;

import fitnesschecker.FitnessChecker;
import genetic.genome.Genome;
import genetic.util.GenomeBuilder;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class PoolZombiesTest {
    @Test
    public void testNewGenerationRemoveWeak() throws Exception {
    ///
        PoolZombies poolZombies = new PoolZombies();

        FitnessChecker fitnessChecker = Mockito.mock(FitnessChecker.class);
        // all new genomes will have better fitness
        when(fitnessChecker.score(any())).thenReturn(300L);
        Species species = new Species(fitnessChecker);
        for (int i = 0; i < 300; i++) {
            species.addGenome(new GenomeBuilder().setFitness(0).build());
        }
        for (int i = 0; i < 700; i++) {
            species.addGenome(new GenomeBuilder().setFitness(100).build());
        }

        poolZombies.getSpeciesPool().clear();
        poolZombies.getSpeciesPool().add(species);
        poolZombies.newGeneration();

        assertThat(allGenomes(poolZombies).stream().filter(gen->gen.getFitness()==0).count(), is(300-250));
    }

    @Test
    public void testNewGenerationRepresentant() throws Exception {

    }

    @Test
    public void testNewGenerationAddToSpecies() throws Exception {

    }

    @Test
    public void testNewGenerationEverySpeciesUsed() throws Exception {

    }

    @Test
    public void testNewGenerationRemoveEmptySpecies() throws Exception {

    }

    @Test
    public void testTopGenomesTakenWithoutMutation() throws Exception {


    }

    @Test
    public void testNewGenerationPopulationConstant() throws Exception {
        PoolZombies pool = new PoolZombies();
        assertThat(allGenomes(pool).size(), is(PoolZombies.POPULATION));

        for (int i = 0; i < 5; i++) {
            pool.newGeneration();
        }

        assertThat(allGenomes(pool).size(), is(PoolZombies.POPULATION));
    }

    private List<Genome> allGenomes(PoolZombies pool) {
        List<Genome> resultSet = new LinkedList<>();
        for (Species species : pool.getSpeciesPool()) {
            resultSet.addAll(species.getGenomes());
        }
        return resultSet;
    }

}
