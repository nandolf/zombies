package genetic;

import com.google.common.collect.Sets;
import genetic.genome.*;
import genetic.neuronalnetwork.NetworkMakerZombies;
import genetic.util.GenomeBuilder;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RandomConstants.class)
public class GenomeTest {

    private static final double c1 = 1;
    private static final double c2 = 1;
    private static final double c3 = 0.4;

    @Test
    public void testNoOtherNeuronCreated() throws Exception {
        Genome genome = new Genome(3, 2);

        assertThat(genome.getNodeGenes().size(), is(3/* Sensors */ + 1 /* Bias */ + 2 /* output */));
        nodeNumberInRange(genome);
    }

    private void nodeNumberInRange(Genome genome) {
        for (NodeGene node : genome.getNodeGenes()) {
            assertTrue(node.getNumber() > 0);
            assertTrue(node.getNumber() <= genome.getNodeGenes().size());
        }
    }

    @Test
    public void testSpeciesDifferenceSame() throws Exception {
        Genome genome = new Genome(0, 2);
        genome.getConnectionGenes().add(new ConnectionGene(1, 2, 1, true, 1));

        assertThat(genome.geneticDistance(genome), is(0.0));
    }

    @Test
    public void testSpeciesDifferenceExpressed() throws Exception {
        double difference = getSpeciesDifference(Arrays.asList(1), Arrays.asList(1, 2));
        assertThat(difference, is(c1 * 1 + 0 + 0));
    }

    @Test
    public void testSpeciesNoSameInnovationNumber() throws Exception {
        double difference = getSpeciesDifference(Arrays.asList(1), Arrays.asList(2));
        assertThat(difference, is(c1 * 1 + c2 * 1 + 0));
    }

    @Test
    public void testDisjoint() throws Exception {
        double difference = getSpeciesDifference(Arrays.asList(1, 3), Arrays.asList(2));
        assertThat(difference, is(c1 * 1 + c2 * 2 + 0));
    }

    @Test
    public void testNoConnectionBoth() {
        double difference = getSpeciesDifference(Arrays.asList(), Arrays.asList());
        assertThat(difference, is(0.0));
    }

    @Test
    public void testNoConnectionOne() {
        double difference = getSpeciesDifference(Arrays.asList(1), Arrays.asList());
        assertThat(difference, is(1.0));
    }

    @Test
    public void testLargeGenome() {
        double difference = getSpeciesDifference(
                Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 26, 27),
                Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25));
        assertThat(difference, is(c1 * 2 / (22.0 / 21) + c2 * 1 / (22.0 / 21) + 0));
    }

    @Test
    public void testWeightDifferenceGenome() {
        Genome genome = new Genome(0, 2);
        genome.getConnectionGenes().add(new ConnectionGene(1, 2, 5, true, 1));
        genome.getConnectionGenes().add(new ConnectionGene(1, 2, 4, true, 2));
        Genome genomeOther = new Genome(0, 2);
        genomeOther.getConnectionGenes().add(new ConnectionGene(1, 2, -2, true, 1));
        genomeOther.getConnectionGenes().add(new ConnectionGene(1, 2, 1, true, 2));

        double difference = genome.geneticDistance(genomeOther);
        assertThat(difference, is(c3 * 5));
    }

    @Test
    public void testWeightDifferenceGenome_SameDiffence() {
        Genome genome = new Genome(0, 2);
        genome.getConnectionGenes().add(new ConnectionGene(1, 2, 5, true, 1));
        genome.getConnectionGenes().add(new ConnectionGene(1, 2, 5, true, 2));
        Genome genomeOther = new Genome(0, 2);
        genomeOther.getConnectionGenes().add(new ConnectionGene(1, 2, -2, true, 1));
        genomeOther.getConnectionGenes().add(new ConnectionGene(1, 2, -2, true, 2));

        double difference = genome.geneticDistance(genomeOther);
        assertThat(difference, is(c3 * 7));
    }

    private double getSpeciesDifference(Collection<Integer> innovationNumbersA,
                                        Collection<Integer> innovationNumbersB) {
        Genome genome = new Genome(0, 2);
        innovationNumbersA.forEach(innovationNumber -> genome.getConnectionGenes()
                .add(new ConnectionGene(1, 2, 1, true, innovationNumber)));
        Genome genomeOther = new Genome(0, 2);
        innovationNumbersB.forEach(innovationNumber -> genomeOther.getConnectionGenes()
                .add(new ConnectionGene(1, 2, 1, true, innovationNumber)));

        return genome.geneticDistance(genomeOther);
    }

    @Test
    public void testAddRandomConnection() throws Exception {
        Genome genome;
        do {
            genome = new Genome(0, 2);
            assertThat(genome.getConnectionGenes().size(), is(0));

            genome.addRandomConnection();
            genome.addRandomConnection();

            assertThat(genome.getConnectionGenes().size(), is(2));
        } while (NetworkMakerZombies.generateNetwork(genome, Collections.emptyList()).hasCycle());
        NetworkMakerZombies.generateNetwork(genome, Collections.emptyList()).getAngleOutput();
    }

    @Test
    public void testAddRandomNodeNoConnection() throws Exception {
        Genome genome = new Genome(0, 2);
        genome.addNodeOnRandomConnection();

        // new connection + 2 node insert connections
        assertThat(genome.getConnectionGenes().size(), is(3));
    }

    @Test
    public void testAddRandomNode() throws Exception {
        Genome genome = new Genome(0, 2);
        // bias to length
        ConnectionGene connection = new ConnectionGene(1, 3, 0.1, true,
                InnovationNumberGenerator.getInstance().getNextInnovationNumber());
        genome.getConnectionGenes().add(connection);
        genome.addNodeOnRandomConnection();
        assertThat(connection.isEnabled(), is(false));
        assertThat(genome.getConnectionGenes().size(), is(3));
        assertTrue(hasConnection(genome, 1, 4, 1.0, true));
        assertTrue(hasConnection(genome, 4, 3, 0.1, true));

        NetworkMakerZombies.generateNetwork(genome, Collections.emptyList());
    }

    private boolean hasConnection(Genome genome, int in, int out, double weight, boolean enabled) {
        for (ConnectionGene connection : genome.getConnectionGenes()) {
            if (connection.getIn() == in)
                if (connection.getOut() == out)
                    if (connection.getWeight() == weight)
                        if (connection.isEnabled() == enabled)
                            return true;
        }
        return false;
    }

    @Test
    public void testMutateConnection() throws Exception {
        // 90% zieht => innerhalb von +/- 1
        mockRandomEvent(RandomConstants.newRandomWeight, false);
        Genome genome = new GenomeBuilder().addConnection(1, 2, 1.0, true, 1).build();
        genome.mutateConnection();

        ConnectionGene connectionGene = genome.getConnectionGenes().stream().findFirst().get();
        assertThat(connectionGene.getWeight(), Matchers.greaterThanOrEqualTo(0.0));
        assertThat(connectionGene.getWeight(), Matchers.lessThanOrEqualTo(2.0));
        assertThat(connectionGene.isEnabled(), is(true));
        assertThat(connectionGene.getIn(), is(1));
        assertThat(connectionGene.getOut(), is(2));
        assertThat(connectionGene.getInnovationNumber(), is(1L));
    }

    @Test
    public void testMutateConnection_noConnection() throws Exception {
        Genome genome = new Genome(0, 2);
        genome.mutateConnection();

        assertThat(genome.getConnectionGenes().size(), is(1));
    }

    @Test
    public void testSelfeCrossover() throws Exception {
        Genome genome = new GenomeBuilder()
                .addConnection(1, 2, 1.0, true, 1)
                .addConnection(1, 3, 0.5, false, 2)
                .build();

        Genome child = genome.crossover(genome);
        assertThat(child.geneticDistance(genome), is(0.0));
    }

    @Test
    public void testFittestCrossover() throws Exception {
        ConnectionGene conSame = new ConnectionGene(1, 2, 1.0, true, 2);

        ConnectionGene conDisjoint = new ConnectionGene(1, 4, 1.0, true, 1);
        ConnectionGene conSame2 = new ConnectionGene(4, 5, 0.67, true, 2);
        ConnectionGene conExcess = new ConnectionGene(5, 2, 24.0, true, 3);

        Genome gen1 = new GenomeBuilder()
                .addConnection(conSame)
                .setFitness(5)
                .build();

        Genome gen2 = new GenomeBuilder()
                .addNode(Type.HIDDEN, 4)
                .addNode(Type.HIDDEN, 5)
                .addConnection(conSame2)
                .addConnection(conDisjoint)
                .addConnection(conExcess)
                .setFitness(10).build();
        Genome child = gen1.crossover(gen2);

        assertThat(child.getConnectionGenes().size(), is(3));
        assertTrue(child.getConnectionGenes().contains(conSame) || child.getConnectionGenes().contains(conSame2));
        assertTrue(child.getConnectionGenes().contains(conExcess));
        assertTrue(child.getConnectionGenes().contains(conDisjoint));
    }

    @Test
    public void testSameFitnessCrossover() throws Exception {
        // while (true) {
        ConnectionGene conSame = new ConnectionGene(1, 2, 1.0, true, 2);

        ConnectionGene conDisjoint = new ConnectionGene(1, 4, 1.0, true, 1);
        ConnectionGene conSame2 = new ConnectionGene(4, 5, 0.67, true, 2);
        ConnectionGene conExcess = new ConnectionGene(5, 2, 24.0, true, 3);

        Genome gen1 = new GenomeBuilder()
                .addConnection(conSame)
                .setFitness(5)
                .build();

        Genome gen2 = new GenomeBuilder()
                .addNode(Type.HIDDEN, 4)
                .addNode(Type.HIDDEN, 5)
                .addConnection(conSame2)
                .addConnection(conDisjoint)
                .addConnection(conExcess)
                .setFitness(5).build();
        Genome child = gen1.crossover(gen2);

        Set<Genome> solutions = Sets.newHashSet(new GenomeBuilder().addConnection(conSame).build(),
                new GenomeBuilder()
                        .addConnection(conSame2)
                        .addNode(Type.HIDDEN, 4)
                        .addNode(Type.HIDDEN, 5)
                        .build(),
                new GenomeBuilder()
                        .addConnection(conDisjoint)
                        .addConnection(conSame)
                        .addNode(Type.HIDDEN, 4)
                        .build(),
                new GenomeBuilder()
                        .addConnection(conDisjoint)
                        .addConnection(conSame2)
                        .addNode(Type.HIDDEN, 4)
                        .addNode(Type.HIDDEN, 5)
                        .build(),
                new GenomeBuilder()
                        .addConnection(conDisjoint)
                        .addConnection(conSame)
                        .addConnection(conExcess)
                        .addNode(Type.HIDDEN, 4)
                        .addNode(Type.HIDDEN, 5)
                        .build(),
                new GenomeBuilder()
                        .addConnection(conDisjoint)
                        .addConnection(conSame2)
                        .addNode(Type.HIDDEN, 4)
                        .addNode(Type.HIDDEN, 5)
                        .addConnection(conExcess)
                        .build(),
                new GenomeBuilder()
                        .addConnection(conExcess)
                        .addConnection(conSame)
                        .addNode(Type.HIDDEN, 5)
                        .build(),
                new GenomeBuilder()
                        .addConnection(conExcess)
                        .addConnection(conSame2)
                        .addNode(Type.HIDDEN, 4)
                        .addNode(Type.HIDDEN, 5)
                        .build());

        if (!solutions.contains(child)) {
            System.out.println(child);
            System.out.println(solutions);
        }
        assertTrue(solutions.contains(child));
    }

    @Test
    public void testOneHasNoConnectionsCrossover() throws Exception {
        Genome genome = new GenomeBuilder().build();
        Genome child = genome.crossover(genome);
        assertThat(child.geneticDistance(genome), is(0.0));
    }

    @Test
    public void testDisableRule_bothDisabledBothEnabled() throws Exception {
        ConnectionGene enableConnection = new ConnectionGene(1, 2, 1, true, 1);
        ConnectionGene disableConnection = new ConnectionGene(1, 3, 1.0, false, 2);

        Genome genome = new GenomeBuilder()
                .addConnection(enableConnection)
                .addConnection(disableConnection)
                .build();

        Genome child = genome.crossover(genome);
        // gleiche innonumber, keins disabled => keins
        assertTrue(child.getConnectionGenes().contains(enableConnection));
        // gleiche innonumber, beide disabled => disable
        assertTrue(child.getConnectionGenes().contains(disableConnection));
    }

    @Test
    public void testDisableRule_sameInno_eitherDisabledWeaker() throws Exception {
        // gleiche innonumber, eins disabled schw�cher => 0.75 disable
        Genome gnomeWeaker = new GenomeBuilder()
                .addConnection(1, 3, 1.0, false, 1)
                .setFitness(0)
                .build();

        Genome gnomeStronger = new GenomeBuilder()
                .addConnection(1, 2, 1, true, 1)
                .setFitness(1)
                .build();

        // disable
        mockRandomEvent(RandomConstants.disableIfEitherDisabled, true);

        Genome child = gnomeWeaker.crossover(gnomeStronger);
        assertThat(child.getConnectionGenes().stream().findFirst().get().isEnabled(),
                is(false));

        // do not disable
        mockRandomEvent(RandomConstants.disableIfEitherDisabled, false);

        child = gnomeWeaker.crossover(gnomeStronger);
        assertThat(child.getConnectionGenes().stream().findFirst().get().isEnabled(),
                is(true));
    }

    @Test
    public void testDisableRule_sameInno_eitherDisabledStronger() throws Exception {
        // gleiche innonumber, eins disabled st�rker => 0.75 disable
        Genome gnomeWeaker = new GenomeBuilder()
                .addConnection(1, 3, 1.0, true, 1)
                .setFitness(0)
                .build();

        Genome gnomeStronger = new GenomeBuilder()
                .addConnection(1, 2, 1, false, 1)
                .setFitness(1)
                .build();

        // disable
        mockRandomEvent(RandomConstants.disableIfEitherDisabled, true);

        Genome child = gnomeWeaker.crossover(gnomeStronger);
        assertThat(child.getConnectionGenes().stream().findFirst().get().isEnabled(),
                is(false));

        // do not disable
        mockRandomEvent(RandomConstants.disableIfEitherDisabled, false);

        child = gnomeWeaker.crossover(gnomeStronger);
        assertThat(child.getConnectionGenes().stream().findFirst().get().isEnabled(),
                is(true));
    }

    @Test
    public void testDisableRule_excess_eitherDisabled() throws Exception {
        // unterschiedliche inno, fittest disabled => 0,75 disable
        Genome genome = new GenomeBuilder()
                .addConnection(1, 3, 1.0, true, 1)
                .addConnection(1, 2, 1, false, 2)
                .setFitness(1)
                .build();

        Genome genomeOther = new GenomeBuilder()
                .addConnection(1, 3, 1.0, true, 1)
                .setFitness(0)
                .build();

        mockRandomEvent(RandomConstants.disableIfEitherDisabled, true);
        Genome child = genome.crossover(genomeOther);
        assertTrue(child.getConnectionGenes().contains(new ConnectionGene(1, 2, 1, false, 2)));

        mockRandomEvent(RandomConstants.disableIfEitherDisabled, false);
        child = genome.crossover(genomeOther);
        assertTrue(child.getConnectionGenes().contains(new ConnectionGene(1, 2, 1, true, 2)));
    }

    @Test
    public void testDisableRule_disjoint_bothEnabled() throws Exception {
        // unterschiedliche inno, fittest enabled => enabled
        Genome genome = new GenomeBuilder()
                .addConnection(1, 3, 1.0, true, 1)
                .setFitness(1)
                .build();

        Genome genomeOther = new GenomeBuilder()
                .setFitness(0)
                .build();

        mockRandomEvent(RandomConstants.disableIfEitherDisabled, true);
        Genome child = genome.crossover(genomeOther);
        assertTrue(child.getConnectionGenes().contains(new ConnectionGene(1, 3, 1, true, 1)));
    }

    private void mockRandomEvent(double event, boolean result) {
        PowerMockito.mockStatic(RandomConstants.class);
        when(RandomConstants.eventTrue(eq(event))).thenReturn(result);
    }
}