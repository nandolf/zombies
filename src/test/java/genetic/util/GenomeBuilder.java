package genetic.util;

import genetic.genome.ConnectionGene;
import genetic.genome.Genome;
import genetic.genome.NodeGene;
import genetic.genome.Type;

import java.util.ArrayList;
import java.util.List;

public class GenomeBuilder {
    private List<NodeGene> nodeGenes = new ArrayList<>();
    private List<ConnectionGene> connectionGenes = new ArrayList<>();
    private long fitness;
    private int senors;

    public GenomeBuilder addNode(Type type, int number) {
        nodeGenes.add(new NodeGene(type, number));
        return this;
    }

    public GenomeBuilder addConnection(int in, int out, double weight, boolean enabled, long innovationNumber) {
        connectionGenes.add(new ConnectionGene(in, out, weight, enabled, innovationNumber));
        return this;
    }

    public GenomeBuilder addConnection(ConnectionGene con) {
        connectionGenes.add(con);
        return this;
    }

    public GenomeBuilder setFitness(long fit) {
        this.fitness = fit;
        return this;
    }

    public GenomeBuilder setSenors(int senors) {
        this.senors = senors;
        return this;
    }

    public Genome build() {
        Genome genome = new Genome(senors, 2);
        genome.getConnectionGenes().addAll(connectionGenes);
        genome.getNodeGenes().addAll(nodeGenes);
        genome.setFitness(fitness);
        return genome;
    }
}