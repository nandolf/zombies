package genetic;

import fitnesschecker.FitnessChecker;
import genetic.genome.ConnectionGene;
import genetic.genome.Genome;
import genetic.util.GenomeBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RandomConstants.class)
public class SpeciesTest {
    @Test
    public void testAdjustedFitness() throws Exception {
        Genome genome = new GenomeBuilder().setFitness(19).build();
        Genome genome2 = new GenomeBuilder().setFitness(10).build();

        Species species = new Species(null);
        species.addGenome(genome);
        species.addGenome(genome2);

        assertThat(species.getAdjustetFitness(genome), is(9.5));
        assertThat(species.getAdjustetFitness(genome2), is(5.0));
    }

    @Test
    public void testReproduce_Crossover() throws Exception {
        FitnessChecker fitnessCheckerMock = Mockito.mock(FitnessChecker.class);
        when(fitnessCheckerMock.score(any())).thenReturn(9L);

        Species species = new Species(fitnessCheckerMock);
        Genome genome1 = new Genome(0, 2);
        Genome genome2 = new GenomeBuilder().addConnection(1, 2, 1, true, 1).setFitness(1).build();
        species.addGenome(genome1);
        species.addGenome(genome2);

        mockRandomEvent(RandomConstants.mutationInsteadOfCrossover, false);
        Genome child = species.reproduce();

        assertThat(child.getFitness(), is(9L));
        child.setFitness(0);// manual crossover doesn't set fitness
        assertThat(child, is(genome1.crossover(genome2)));
    }

    @Test
    public void testReproduce_Crossover_OnlyOneGenome() throws Exception {
        FitnessChecker fitnessCheckerMock = Mockito.mock(FitnessChecker.class);
        when(fitnessCheckerMock.score(any())).thenReturn(0L);

        Species species = new Species(fitnessCheckerMock);
        species.addGenome(new Genome(0, 2));

        mockRandomEvent(RandomConstants.mutationInsteadOfCrossover, false);
        species.reproduce();
    }

    @Test
    public void testReproduce_Mutation_mutateLink() throws Exception {
        FitnessChecker fitnessCheckerMock = Mockito.mock(FitnessChecker.class);
        when(fitnessCheckerMock.score(any())).thenReturn(42L);

        Species species = new Species(fitnessCheckerMock);
        Genome genome = new GenomeBuilder().setFitness(0).addConnection(1, 2, 1.0, true, 1).build();
        species.addGenome(genome);

        PowerMockito.mockStatic(RandomConstants.class);
        when(RandomConstants.eventTrue(eq(RandomConstants.mutationInsteadOfCrossover))).thenReturn(true);
        when(RandomConstants.eventTrue(eq(RandomConstants.addingNodePopulation))).thenReturn(false);
        when(RandomConstants.eventTrue(eq(RandomConstants.addingLinkPopulation))).thenReturn(false);

        Genome child = species.reproduce();
        assertThat(child.getConnectionGenes().size(), is(1));
        ConnectionGene connectionGene = child.getConnectionGenes().stream().filter(conn -> conn.getInnovationNumber() == 1)
                .findFirst()
                .get();
        assertThat(connectionGene.getWeight(), not(is(1.0)));
        assertThat(child.getFitness(), is(42L));
    }

    @Test
    public void testReproduce_Mutation_addLink() throws Exception {
        FitnessChecker fitnessCheckerMock = Mockito.mock(FitnessChecker.class);
        when(fitnessCheckerMock.score(any())).thenReturn(42L);

        Species species = new Species(fitnessCheckerMock);
        species.addGenome(new Genome(0, 2));

        PowerMockito.mockStatic(RandomConstants.class);
        when(RandomConstants.eventTrue(eq(RandomConstants.mutationInsteadOfCrossover))).thenReturn(true);
        when(RandomConstants.eventTrue(eq(RandomConstants.addingNodePopulation))).thenReturn(false);
        when(RandomConstants.eventTrue(eq(RandomConstants.addingLinkPopulation))).thenReturn(true);

        Genome child = species.reproduce();
        assertThat(child.getConnectionGenes().size(), is(1));
        assertThat(child.getFitness(), is(42L));
    }

    @Test
    public void testReproduce_Mutation_addNode() throws Exception {
        FitnessChecker fitnessCheckerMock = Mockito.mock(FitnessChecker.class);
        when(fitnessCheckerMock.score(any())).thenReturn(42L);

        Species species = new Species(fitnessCheckerMock);
        species.addGenome(new Genome(0, 2));

        PowerMockito.mockStatic(RandomConstants.class);
        when(RandomConstants.eventTrue(eq(RandomConstants.mutationInsteadOfCrossover))).thenReturn(true);
        when(RandomConstants.eventTrue(eq(RandomConstants.addingNodePopulation))).thenReturn(true);
        when(RandomConstants.eventTrue(eq(RandomConstants.addingLinkPopulation))).thenReturn(false);

        Genome child = species.reproduce();
        assertThat(child.getConnectionGenes().size(), is(3));
        assertThat(child.getFitness(), is(42L));
    }

    private void mockRandomEvent(double event, boolean result) {
        PowerMockito.mockStatic(RandomConstants.class);
        when(RandomConstants.eventTrue(eq(event))).thenReturn(result);
    }

    @Test
    public void testChooseRepresentant() throws Exception {
        Species species = new Species(null);
        Genome representantGenome = new GenomeBuilder().addConnection(1, 2, 1, true, 1).build();
        species.addGenome(representantGenome);
        species.chooseRepresentant();

        species.getGenomes().remove(representantGenome);
        species.addGenome(new Genome(0, 2));

        assertThat(species.getRepresentant(), is(representantGenome));
    }

    @Test
    public void testMutateWithBadFitness() throws Exception {
        // fitnesschecker liefert -1 f�r mutation => new genome muss neu gecloned werden
        // TODO vllt auch f�r crossover

    }
}
