package genetic.neuronalnetwork;

import com.google.common.collect.Lists;
import data.Point2D;
import genetic.genome.Genome;
import genetic.sensors.BlockSensor;
import genetic.util.GenomeBuilder;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NetworkZombiesTest {
    @Test
    public void testSetPlayer_firstSensor() {
        // bias 1
        // sensor 2,3,4
        // out 5,6
        // sensor 2 -> output 5
        // sensor 3,4 -> output 6
        Genome genome = new GenomeBuilder()
                .setSenors(3)
                .addConnection(2, 5, 1.0, true, 1)
                .addConnection(3, 6, 1, true, 1)
                .addConnection(4, 6, 1, true, 1).build();

        BlockSensor playerSensor = new BlockSensor(new Point2D(0, 0), 1);
        BlockSensor humanSensor = new BlockSensor(new Point2D(0, 0), 1);
        BlockSensor zombieSensor = new BlockSensor(new Point2D(0, 0), 1);

        NetworkZombies network = NetworkMakerZombies.generateNetwork(genome, Lists.newArrayList(playerSensor, humanSensor, zombieSensor));
        network.setPlayer(new Point2D(0, 0));

        assertThat(network.getLengthOutput(), is(0.0));
        assertThat(network.getAngleOutput(), is(1.0));
    }

    @Test
    public void testSetHumans() {
        // bias 1
        // sensor 2,3,4
        // out 5,6
        // sensor 3 -> output 5
        // sensor 2(player),4(zombie) -> output 6
        Genome genome = new GenomeBuilder()
                .setSenors(3)
                .addConnection(3, 5, 1.0, true, 1)
                .addConnection(2, 6, 1, true, 1)
                .addConnection(4, 6, 1, true, 1).build();

        BlockSensor playerSensor = new BlockSensor(new Point2D(0, 0), 1);
        BlockSensor humanSensor = new BlockSensor(new Point2D(0, 0), 1);
        BlockSensor zombieSensor = new BlockSensor(new Point2D(0, 0), 1);

        NetworkZombies network = NetworkMakerZombies.generateNetwork(genome, Lists.newArrayList(playerSensor, humanSensor, zombieSensor));
        network.setHumans(Collections.singletonList(new Point2D(0, 0)));

        assertThat(network.getLengthOutput(), is(0.0));
        assertThat(network.getAngleOutput(), is(1.0));
    }

    @Test
    public void testSetZombies() {
        // bias 1
        // sensor 2,3,4
        // out 5,6
        // sensor 4 (zombies)-> output 5
        // sensor 2,3 -> output 6
        Genome genome = new GenomeBuilder()
                .setSenors(3)
                .addConnection(4, 5, 1.0, true, 1)
                .addConnection(2, 6, 1, true, 1)
                .addConnection(3, 6, 1, true, 1).build();

        BlockSensor playerSensor = new BlockSensor(new Point2D(0, 0), 1);
        BlockSensor humanSensor = new BlockSensor(new Point2D(0, 0), 1);
        BlockSensor zombieSensor = new BlockSensor(new Point2D(0, 0), 1);

        NetworkZombies network = NetworkMakerZombies.generateNetwork(genome, Lists.newArrayList(playerSensor, humanSensor, zombieSensor));
        network.setZombies(Collections.singletonList(new Point2D(0, 0)));

        assertThat(network.getLengthOutput(), is(0.0));
        assertThat(network.getAngleOutput(), is(1.0));
    }

    @Test
    public void testSetPlayer_secondSensor() {
        // bias 1
        // sensor 2,3,4,5,6,7
        // out 8,9
        // sensor 5(player) -> output 8
        // sensor 2-7(au�er 5) -> output 9
        Genome genome = new GenomeBuilder()
                .setSenors(6)
                .addConnection(5, 8, 1.0, true, 1)
                .addConnection(2, 9, 1, true, 1)
                .addConnection(3, 9, 1, true, 1)
                .addConnection(4, 9, 1, true, 1)
                .addConnection(6, 9, 1, true, 1)
                .addConnection(7, 9, 1, true, 1)
                .build();

        BlockSensor playerSensor = new BlockSensor(new Point2D(0, 0), 1);
        BlockSensor humanSensor = new BlockSensor(new Point2D(0, 0), 1);
        BlockSensor zombieSensor = new BlockSensor(new Point2D(0, 0), 1);

        BlockSensor playerSensor2 = new BlockSensor(new Point2D(1, 0), 1);
        BlockSensor humanSensor2 = new BlockSensor(new Point2D(1, 0), 1);
        BlockSensor zombieSensor2 = new BlockSensor(new Point2D(1, 0), 1);


        NetworkZombies network = NetworkMakerZombies.generateNetwork(
                genome, Lists.newArrayList(playerSensor, humanSensor, zombieSensor, playerSensor2, humanSensor2, zombieSensor2));
        network.setPlayer(new Point2D(1, 0));

        assertThat(network.getLengthOutput(), is(0.0));
        assertThat(network.getAngleOutput(), is(1.0));
    }

}