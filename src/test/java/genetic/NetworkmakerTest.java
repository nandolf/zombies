package genetic;

import com.google.common.collect.Lists;
import data.Point2D;
import genetic.genome.ConnectionGene;
import genetic.genome.Genome;
import genetic.genome.NodeGene;
import genetic.genome.Type;
import genetic.neuronalnetwork.NetworkMakerZombies;
import genetic.neuronalnetwork.NetworkZombies;
import genetic.neuronalnetwork.Neuron;
import genetic.sensors.BlockSensor;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class NetworkmakerTest {
    @Test
    public void testCreateNetwork_NoHidden() throws Exception {
        List<BlockSensor> sensors = Arrays.asList(new BlockSensor(new Point2D(0, 0), 1),
                new BlockSensor(new Point2D(1.0, 1.0), 1), new BlockSensor(new Point2D(0.0, 1.0), 1));

        Genome genome = new Genome(3, 2);
        // bias 1
        // sensor 2,3,4
        // out 5,6
        // sensor 2 -> out 5
        // sensor 4 -> out 6
        genome.getConnectionGenes().add(new ConnectionGene(2, 5, 1.0, true, 1));
        genome.getConnectionGenes().add(new ConnectionGene(4, 6, 1.0, true, 2));
        NetworkZombies network = NetworkMakerZombies.generateNetwork(genome, sensors);

        sensors.forEach(sensor -> sensor.setFigures(Arrays.asList(new Point2D(0, 0), new Point2D(1, 1), new Point2D(0, 1))));
        assertThat(network.getAngleOutput(), is(Neuron.defaultFunction.output(1)));
        assertThat(network.getLengthOutput(), is(Neuron.defaultFunction.output(1)));
        nodeNumberInRange(genome);
    }

    @Test
    public void testCreateNetwork_WithHidden() throws Exception {
        List<BlockSensor> sensors = Lists.newArrayList(new BlockSensor(new Point2D(0, 0), 1),
                new BlockSensor(new Point2D(1.0, 1.0), 1), new BlockSensor(new Point2D(0.0, 1.0), 1));

        // bias 1
        // sensor 2,3,4
        // out 5,6
        // neuron 7
        // sensor 2 -> neuron 7 -> out 5
        Genome genome = new Genome(3, 2);
        genome.getConnectionGenes().add(new ConnectionGene(2, 7, 1.0, true, 1));
        genome.getConnectionGenes().add(new ConnectionGene(7, 5, 1.0, true, 2));
        genome.getNodeGenes().add(new NodeGene(Type.HIDDEN, 7));
        NetworkZombies network = NetworkMakerZombies.generateNetwork(genome, sensors);

        sensors.forEach(sensor -> sensor.setFigures(Arrays.asList(new Point2D(0, 0), new Point2D(1, 1))));
        assertThat(network.getAngleOutput(), is(Neuron.defaultFunction.output(Neuron.defaultFunction.output(1))));
        assertThat(network.getLengthOutput(), is(Neuron.defaultFunction.output(0)));
        nodeNumberInRange(genome);
    }

    @Test
    public void testCreateNetwork_WithHiddenDisabled() throws Exception {
        List<BlockSensor> sensors = Arrays.asList(new BlockSensor(new Point2D(0, 0), 1),
                new BlockSensor(new Point2D(1.0, 1.0), 1), new BlockSensor(new Point2D(0.0, 1.0), 1));


        // bias 1
        // sensor 2,3,4
        // out 5,6
        // neuron 7
        // sensor 2 -> neuron 7 -> out 4
        Genome genome = new Genome(3, 2);
        genome.getConnectionGenes().add(new ConnectionGene(2, 7, 1.0, true, 1));
        genome.getConnectionGenes().add(new ConnectionGene(7, 4, 1.0, false, 2));
        genome.getNodeGenes().add(new NodeGene(Type.HIDDEN, 7));

        NetworkZombies network = NetworkMakerZombies.generateNetwork(genome, sensors);

        sensors.forEach(sensor -> sensor.setFigures(Arrays.asList(new Point2D(0, 0), new Point2D(1, 1), new Point2D(0, 1))));
        assertThat(network.getAngleOutput(), is(Neuron.defaultFunction.output(0)));
        assertThat(network.getLengthOutput(), is(Neuron.defaultFunction.output(0)));
        nodeNumberInRange(genome);
    }

    @Test
    public void testCreateNetwork_2ConnectionsForSame() throws Exception {
        // TODO: in Netzwerkcreator falls herausgezogen. genome macht mit random
        // add keine doppelten
    }

    @Test(expected = IllegalStateException.class)
    public void testConnectionToNonExistingNode_Out() throws Exception {
        Genome genome = new Genome(0, 2);
        genome.getConnectionGenes().add(new ConnectionGene(1, 9, 1.0, true, 1));
        NetworkMakerZombies.generateNetwork(genome, Collections.emptyList());
    }

    @Test(expected = IllegalStateException.class)
    public void testConnectionToNonExistingNode_In() throws Exception {
        Genome genome = new Genome(0, 2);
        genome.getConnectionGenes().add(new ConnectionGene(9, 1, 1.0, true, 1));
        NetworkMakerZombies.generateNetwork(genome, Collections.emptyList());
    }

    @Test(expected = IllegalStateException.class)
    public void testConnectionToSensor() throws Exception {
        // outAngle -> bias
        Genome genome = new Genome(0, 2);
        genome.getConnectionGenes().add(new ConnectionGene(2, 1, 1.0, true, 1));
        NetworkMakerZombies.generateNetwork(genome, Collections.emptyList());
    }

    private void nodeNumberInRange(Genome genome) {
        for (NodeGene node : genome.getNodeGenes()) {
            assertTrue(node.getNumber() > 0);
            assertTrue(node.getNumber() <= genome.getNodeGenes().size());
        }
    }

}
