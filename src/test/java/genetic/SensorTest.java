package genetic;

import data.Point2D;
import genetic.sensors.BlockSensor;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class SensorTest {
	private static final int BLOCKSIZE = 100;

	@Test
	public void testBlockSensorInside() throws Exception {
		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(Arrays.asList(new Point2D(50, 50)));
		assertThat(blockSensor.getOutput(), is(1.0));
	}

	@Test
	public void testMultipleFiguresOnSensor() throws Exception {
		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(Arrays.asList(new Point2D(50, 50), new Point2D(50,50)));
		assertThat(blockSensor.getOutput(), is(2.0));
	}

	@Test
	public void testBlockSensorOutside() throws Exception {
		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(Arrays.asList(new Point2D(200, 200)));
		assertThat(blockSensor.getOutput(), is(0.0));
	}

	@Test
	public void testBlockSensorLeftBorder() throws Exception {
		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(Arrays.asList(new Point2D(0, 50)));
		assertThat(blockSensor.getOutput(), is(1.0));
	}

	@Test
	public void testBlockSensorRightBorder() throws Exception {
		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(Arrays.asList(new Point2D(BLOCKSIZE, 50)));
		assertThat(blockSensor.getOutput(), is(0.0));
	}

	@Test
	public void testBlockSensorTopBorder() throws Exception {
		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(Arrays.asList(new Point2D(50, 0)));
		assertThat(blockSensor.getOutput(), is(1.0));
	}

	@Test
	public void testBlockSensorBottomBorder() throws Exception {
		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(Arrays.asList(new Point2D(50, BLOCKSIZE)));
		assertThat(blockSensor.getOutput(), is(0.0));
	}

	@Test
	public void testBlockSensorEdgeRightBottom() throws Exception {
		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(Arrays.asList(new Point2D(BLOCKSIZE, BLOCKSIZE)));
		// N�chster Sensor �bernimmt hier schon
		assertThat(blockSensor.getOutput(), is(0.0));
	}

	@Test
	public void testBlockSensorBorder2Sensors() throws Exception {
		List<Point2D> figures = Arrays.asList(new Point2D(BLOCKSIZE, 0));

		BlockSensor blockSensor = new BlockSensor(new Point2D(0, 0), BLOCKSIZE);
		blockSensor.setFigures(figures);
		BlockSensor blockSensor2 = new BlockSensor(new Point2D(100, 0), BLOCKSIZE);
		blockSensor2.setFigures(figures);

		assertThat(blockSensor.getOutput(), is(0.0));
		assertThat(blockSensor2.getOutput(), is(1.0));
	}
}
