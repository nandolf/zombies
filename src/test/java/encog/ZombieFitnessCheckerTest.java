package encog;

import data.DataProvider;
import org.encog.ml.data.MLData;
import org.encog.neural.neat.NEATNetwork;
import org.junit.Test;
import org.mockito.Mockito;
import simulator.NullPrinter;
import simulator.Simulator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class ZombieFitnessCheckerTest {

    private ZombieFitnessChecker sut = new ZombieFitnessChecker();

    @Test
    public void testScoreAllSzenarios() throws Exception {
        NEATNetwork networkMock = generateIdleNetworkMock();
        long scoreFitnessCheck = sut.score(networkMock);
        long expectedFitness = DataProvider.getAllSzenarios().stream().mapToLong(szenario -> calculateScore(szenario, networkMock)).sum();

        assertThat(scoreFitnessCheck, is(expectedFitness));
    }

    private long calculateScore(DataProvider szenario, NEATNetwork networkMock) {
        Simulator simulator = new Simulator(szenario, new NullPrinter());
        return simulator.simulate(new NeuronStrategy(networkMock));
    }

    private NEATNetwork generateIdleNetworkMock() {
        // TODO Encog ist scheisse um networks zu parsen.
        // Wir brauchen hier ein echtes netzwerk das unterschiedlich auf eingabene reagiert um multithreaded scoring zu testen.
        // PersistNEATPopulation hat die informationen, aber kann keine einzelnen netzwerke erstellen.
        NEATNetwork networkMock = Mockito.mock(NEATNetwork.class);
        MLData outputMock = Mockito.mock(MLData.class);
        when(outputMock.getData(0)).thenReturn(0.0);
        when(outputMock.getData(1)).thenReturn(0.0);
        when(networkMock.compute(any())).thenReturn(outputMock);
        return networkMock;
    }
}