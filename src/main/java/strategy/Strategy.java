package strategy;

import java.util.List;

import data.Point2D;

@FunctionalInterface
public interface Strategy {
	static final int speedPlayer = 1000;
	static final int speedZombie = 400;

	String getNextSysO(Point2D player, List<Point2D> humans, List<Point2D> zombies);
}
