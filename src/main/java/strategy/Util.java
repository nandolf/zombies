package strategy;

import data.Point2D;

import java.util.List;

public class Util {

    public static Point2D nearestFigure(Point2D referenceFigure, List<Point2D> others) {
        return others.stream().min((figureA, figureB) -> Double.compare(figureA.distance(referenceFigure),
                figureB.distance(referenceFigure))).get();
    }

    public static Point2D rotateVector(Point2D vector, double angle) {
        angle = Math.toRadians(angle);
        return new Point2D(vector.getX() * Math.cos(angle) - vector.getY() * Math.sin(angle),
                vector.getX() * Math.sin(angle) + vector.getY() * Math.cos(angle));
    }

    public static boolean pointInField(Point2D point) {
        return point.getX() < 16000 && point.getX() >= 0 && point.getY() < 9000 && point.getY() >= 0;
    }
}
