package strategy;

import data.Point2D;
import genetic.neuronalnetwork.NetworkZombies;

import java.util.List;

public class NeuronStrategy implements Strategy {

    private NetworkZombies network;

    public NeuronStrategy(NetworkZombies network) {
        this.network = network;
    }

    @Override
    public String getNextSysO(Point2D player, List<Point2D> humans, List<Point2D> zombies) {
        network.setPlayer(player);
        network.setZombies(zombies);
        network.setHumans(humans);

        double length = network.getLengthOutput() * speedPlayer;
        double angle = network.getAngleOutput() * 360;

        Point2D direction = Util.rotateVector(new Point2D(0, 1).multiply(length), angle);
        Point2D pointToWalk = player.add(direction);
        return Math.round(pointToWalk.getX()) + " " + Math.round(pointToWalk.getY());
    }

}
