package strategy;

import java.util.List;

import data.Point2D;

public class HumanStrategy implements Strategy {

	@Override
	public String getNextSysO(Point2D player, List<Point2D> humans, List<Point2D> zombies) {
		Point2D figure = humans.stream().findFirst().get();
		return figure.getX() + " " + figure.getY();
	}

}
