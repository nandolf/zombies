package user_interface;

import data.DataProvider;
import simulator.NullPrinter;
import simulator.Simulator;
import strategy.HumanStrategy;
import strategy.Strategy;

import javax.swing.*;
import java.util.LinkedList;
import java.util.List;

public class MainMenu extends JFrame {
    private Spielfeld spielfeld;
    private JButton startSimulationButton;
    private JComboBox<LevelEntry> levelSelection;
    private JPanel mainPanel;
    private JMenuItem evolutionMenuStartNew;
    private JMenuItem evolutionMenuLoadExisting;

    List<LevelEntry> levels = new LinkedList<>();
    private Strategy strategy;

    public MainMenu() {
        setSize(1600, 900);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(mainPanel);
        loadLevels();
        startSimulationButton.addActionListener(e -> new Thread(this::startSimulation).start());
        printData(getCurrentSelectedLevel());
        levelSelection.addActionListener(e -> printData(getCurrentSelectedLevel()));
        setStrategy(new HumanStrategy());

        JMenuBar menu = new JMenuBar();
        JMenu evolution = new JMenu("Evolution");
        menu.add(evolution);
        evolutionMenuStartNew = new JMenuItem("Start new evolution");
        evolution.add(evolutionMenuStartNew);
        evolutionMenuLoadExisting = new JMenuItem("Load existing evolution");
        evolution.add(evolutionMenuLoadExisting);

        setJMenuBar(menu);
        setVisible(true);
    }

    private DataProvider getCurrentSelectedLevel() {
        return ((LevelEntry) levelSelection.getSelectedItem()).getData();
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
        updateLevels();
        startSimulation();
    }

    private void updateLevels() {
        for (LevelEntry level : levels) {
            Simulator sim = new Simulator(level.data, new NullPrinter());
            level.setScore(sim.simulate(strategy));
        }
        levelSelection.repaint();
    }

    private void printData(DataProvider data) {
        spielfeld.print(data.getPlayer(), data.getHumans(), data.getZombies(), 0);
    }

    private void startSimulation() {
        DataProvider data = getCurrentSelectedLevel();
        printData(data);
        if (strategy != null) {
            Simulator simulator = new Simulator(data, spielfeld);
            simulator.simulate(strategy);
        }
    }

    private void loadLevels() {
        DataProvider.getAllSzenarios().forEach(level -> levels.add(new LevelEntry(level)));
        levels.forEach(levelSelection::addItem);
    }

    private void createUIComponents() {
        spielfeld = new Spielfeld();
    }

    public JMenuItem getEvolutionMenuStartNew() {
        return evolutionMenuStartNew;
    }

    public JMenuItem getEvolutionMenuLoadExisting() {
        return evolutionMenuLoadExisting;
    }

    class LevelEntry {
        private String score = "?";
        private DataProvider data;

        LevelEntry(DataProvider data) {
            this.data = data;
        }

        public long getScore() {
            return Long.parseLong(score);
        }

        public DataProvider getData() {
            return data;
        }

        public void setScore(long score) {
            this.score = String.valueOf(score);
        }

        @Override
        public String toString() {
            return data.getLevelName() + ": " + score;
        }
    }

}
