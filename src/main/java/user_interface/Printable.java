package user_interface;

import java.util.List;

import data.Point2D;

@FunctionalInterface
public interface Printable {
	void print(Point2D player, List<Point2D> humans, List<Point2D> zombies, long score);
}