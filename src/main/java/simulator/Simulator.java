package simulator;

import data.DataProvider;
import data.Point2D;
import strategy.Strategy;
import strategy.Util;
import user_interface.Printable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Simulator {
    private List<Point2D> simulatedZombies;
    private List<Point2D> simulatedHumans;
    private Point2D simulatedPlayer;
    private Printable printer;

    public Simulator(DataProvider dataProvider, Printable printer) {
        this.simulatedZombies = dataProvider.getZombies().stream().map(Point2D::new).collect(Collectors.toList());
        this.simulatedHumans = dataProvider.getHumans().stream().map(Point2D::new).collect(Collectors.toList());
        this.simulatedPlayer = new Point2D(dataProvider.getPlayer());
        this.printer = printer;
    }

    public long simulate(Strategy strategy) {
        long score = 0;
        try {
            while (!simulatedZombies.isEmpty()) {
                zombiesWalkToTarget();
                ashMovesToTarget(strategy);
                score += killZombies();
                eatHumans();
                printer.print(simulatedPlayer, simulatedHumans, simulatedZombies, score);
                if (simulatedHumans.isEmpty()) {
                    printer.print(simulatedPlayer, simulatedHumans, simulatedZombies, 0);
                    return 0;
                }
            }
        } catch (SimulatorException e) {
            printer.print(simulatedPlayer, simulatedHumans, simulatedZombies, 0);
            return 0;
        }
        return score;
    }

    private long killZombies() {
        long score = 0;
        long kombo = 1;
        Iterator<Point2D> iterator = simulatedZombies.iterator();
        while (iterator.hasNext()) {
            Point2D zombie = iterator.next();
            if (zombie.distance(simulatedPlayer) < 2000) {
                score += fib(++kombo) * simulatedHumans.size() * simulatedHumans.size() * 10;
                iterator.remove();
            }
        }
        return score;
    }

    private int fib(long kombo) {
        if (kombo == 0)
            return 0;
        if (kombo == 1)
            return 1;
        return fib(kombo - 1) + fib(kombo - 2);
    }

    private void zombiesWalkToTarget() throws SimulatorException {
        List<Point2D> allHumans = new ArrayList<>();
        allHumans.add(simulatedPlayer);
        allHumans.addAll(simulatedHumans);
        for (Point2D zombie : simulatedZombies) {
            Point2D targetHuman = Util.nearestFigure(zombie, allHumans);
            moveTowardsTarget(zombie, targetHuman, Strategy.speedZombie);
        }
    }

    private void ashMovesToTarget(Strategy strategy) throws SimulatorException {
        String nextMove = strategy.getNextSysO(simulatedPlayer, simulatedHumans, simulatedZombies);
        String[] moves = nextMove.split(" ");
        moveTowardsTarget(simulatedPlayer, new Point2D(Double.parseDouble(moves[0]), Double.parseDouble(moves[1])),
                Strategy.speedPlayer);
    }

    private void eatHumans() {
        for (Point2D zombie : simulatedZombies) {
            if (simulatedHumans.isEmpty()) {
                return;
            }
            Point2D human = Util.nearestFigure(zombie, simulatedHumans);
            if (zombie.distance(human) < 1) {
                simulatedHumans.remove(human);
            }
        }
    }

    private void moveTowardsTarget(Point2D player, Point2D target, int speed) throws SimulatorException {
        if (player.distance(target) < speed) {
            player.set(target);
            return;
        }
        Point2D walkDirection = target.subtract(player).normalize();
        Point2D nextPlayerPosition = player.add(walkDirection.multiply(speed));
        player.set(nextPlayerPosition);

        if (!Util.pointInField(nextPlayerPosition))
            throw new SimulatorException("Point out of field");
    }

}
