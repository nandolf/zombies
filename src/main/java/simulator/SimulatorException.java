package simulator;

public class SimulatorException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8738257101711073763L;

	public SimulatorException(String message) {
		super(message);
	}
}
