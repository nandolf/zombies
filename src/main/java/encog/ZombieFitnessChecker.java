package encog;

import data.DataProvider;
import org.encog.neural.neat.NEATNetwork;
import simulator.NullPrinter;
import simulator.Simulator;

public class ZombieFitnessChecker {

    public long score(NEATNetwork network) {
        long score = 0;
        for (DataProvider dataProvider : DataProvider.getAllSzenarios()) {
            score += simulateData(dataProvider, network);
        }
        return score;
    }

    private long simulateData(DataProvider dataProvider, NEATNetwork network) {
        Simulator simulator = new Simulator(dataProvider, new NullPrinter());
        return simulator.simulate(new NeuronStrategy(network));
    }

}
