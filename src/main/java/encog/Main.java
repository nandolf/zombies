package encog;

import org.encog.Encog;
import org.encog.ml.ea.train.EvolutionaryAlgorithm;
import org.encog.neural.neat.NEATNetwork;
import org.encog.neural.neat.NEATPopulation;
import org.encog.neural.neat.NEATUtil;
import org.encog.neural.neat.PersistNEATPopulation;
import strategy.Strategy;
import user_interface.MainMenu;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {

    private static final String LAST_BEST_POPULATION_FILENAME = "lastBestPopulation.population";
    private NEATPopulation population;
    private final MainMenu mainMenu = new MainMenu();
    private static volatile boolean newPopulation = false;

    public Main() {
        mainMenu.getEvolutionMenuStartNew().addActionListener(e -> {
            newPopulation = true;
            new Thread(this::startNewPopulation).start();
        });
        mainMenu.getEvolutionMenuLoadExisting().addActionListener(e -> {
            newPopulation = true;
            new Thread(this::loadExistingPopulation).start();
        });
    }

    public static void main(String[] args) {
        new Main();
    }

    private void doEvolution() {
        newPopulation = false;
        final EvolutionaryAlgorithm train = NEATUtil.constructNEATTrainer(population, new ZombieScoreCalculator());
        double bestScore = 0;
        do {
            train.iteration();
            double score = 1.0 / train.getError();
            System.out.println("Epoch #" + train.getIteration() + " Score:" + score + ", Species:" + population.getSpecies().size());
            if (score > bestScore) {
                System.out.println(train.getBestGenome());
                mainMenu.setStrategy(getCurrentBestStrategy(train));
                bestScore = score;
                writeCurrentPopulationToFile();
            }
        } while (!newPopulation);
        System.out.println("evolution stopped");
        Encog.getInstance().shutdown();
    }

    private void writeCurrentPopulationToFile() {
        try {
            new PersistNEATPopulation().save(new FileOutputStream(new File(LAST_BEST_POPULATION_FILENAME)), population);
        } catch (FileNotFoundException e) {
            showErrorDialog("Unable to create File: " + new File(LAST_BEST_POPULATION_FILENAME).getAbsolutePath());
        }
    }

    private void startNewPopulation() {
        population = new NEATPopulation(SensorUtil.sensorCountAll(), 2, 1000);
        population.setInitialConnectionDensity(1.0);// not required, but speeds training
        population.reset();
        doEvolution();
    }

    private void loadExistingPopulation() {
        File lastPopulationFile = new File(LAST_BEST_POPULATION_FILENAME);
        try {
            population = (NEATPopulation) new PersistNEATPopulation().read(new FileInputStream(lastPopulationFile));
            doEvolution();
        } catch (FileNotFoundException e) {
            showErrorDialog("File: " + lastPopulationFile.getAbsolutePath() + " not found.");
            e.printStackTrace();
        }
    }

    private static Strategy getCurrentBestStrategy(EvolutionaryAlgorithm train) {
        NEATNetwork network = (NEATNetwork) train.getCODEC().decode(train.getBestGenome());
        return new NeuronStrategy(network);
    }

    private void showErrorDialog(String message) {
        JOptionPane.showMessageDialog(mainMenu, message, "Error: ", JOptionPane.ERROR_MESSAGE);
    }
}
