package encog;

import org.encog.ml.CalculateScore;
import org.encog.ml.MLMethod;
import org.encog.neural.neat.NEATNetwork;

public class ZombieScoreCalculator implements CalculateScore {

    @Override
    public double calculateScore(MLMethod mlMethod) {
        NEATNetwork network = (NEATNetwork) mlMethod;
        ZombieFitnessChecker fitnessChecker = new ZombieFitnessChecker();
        return 1.0 / fitnessChecker.score(network);
    }

    @Override
    public boolean shouldMinimize() {
        return true;
    }

    @Override
    public boolean requireSingleThreaded() {
        return false;
    }
}
