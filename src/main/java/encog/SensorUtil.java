package encog;

import data.Point2D;
import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import strategy.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SensorUtil {

    private static final int BLOCKSIZE = 500;
    private static final int GAMEFIELD_WIDTH = 16000;
    private static final int GAMEFIELD_HEIGHT = 9000;

    private Map<Integer, Integer> sensors = new HashMap<>();

    public SensorUtil() {
        resetSensors();
    }

    private void resetSensors() {
        for (int i = 0; i < sensorCountAll(); i++) {
            sensors.put(i, 0);
        }
    }

    public MLData generateInputData(Point2D player, List<Point2D> humans, List<Point2D> zombies) {
        resetSensors();
        MLData input = new BasicMLData(sensorCountAll());

        activateSensorPlayer(player);
        humans.forEach(this::activateSensorHuman);
        zombies.forEach(this::activateSensorZombie);

        for (Map.Entry<Integer, Integer> entry : sensors.entrySet()) {
            input.setData(entry.getKey(), entry.getValue());
        }

        return input;
    }

    public static int sensorCountAll() {
        return sensorCount() * 3;
    }

    private static int sensorCount() {
        if (GAMEFIELD_WIDTH % BLOCKSIZE != 0 || GAMEFIELD_HEIGHT % BLOCKSIZE != 0)
            throw new IllegalStateException("Blocksize not multiple of Gamefieldsize");
        return (GAMEFIELD_WIDTH / BLOCKSIZE) * (GAMEFIELD_HEIGHT / BLOCKSIZE);
    }

    private void activateSensorPlayer(Point2D figure) {
        incrementSensor(figure, 0);
    }

    private void activateSensorHuman(Point2D figure) {
        incrementSensor(figure, 1);
    }

    private void activateSensorZombie(Point2D figure) {
        incrementSensor(figure, 2);
    }

    private void incrementSensor(Point2D figure, int factor) {
        if (!Util.pointInField(figure)) {
            return;
        }

        int index = getSensorIndex(figure) + (sensorCount() * factor);
        Integer currentValue = sensors.get(index);
        sensors.put(index, currentValue + 1);
    }

    private int getSensorIndex(Point2D figure) {
        int rowNumber = (int) figure.getY() / BLOCKSIZE;
        int positionInRow = (int) figure.getX() / BLOCKSIZE;

        if ((int) figure.getX() == GAMEFIELD_WIDTH) {
            positionInRow--;
        }
        if ((int) figure.getY() == GAMEFIELD_HEIGHT) {
            rowNumber--;
        }

        int sensorsPerRow = GAMEFIELD_WIDTH / BLOCKSIZE;
        return rowNumber * sensorsPerRow + positionInRow;
    }

}
