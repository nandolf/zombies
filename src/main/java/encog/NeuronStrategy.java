package encog;

import data.Point2D;
import org.encog.ml.data.MLData;
import org.encog.neural.neat.NEATNetwork;
import strategy.Strategy;
import strategy.Util;

import java.util.List;

public class NeuronStrategy implements Strategy {

    private NEATNetwork network;
    private SensorUtil sensorUtil = new SensorUtil();

    public NeuronStrategy(NEATNetwork network) {
        this.network = network;
    }

    @Override
    public String getNextSysO(Point2D player, List<Point2D> humans, List<Point2D> zombies) {
        MLData result = network.compute(sensorUtil.generateInputData(player, humans, zombies));

        double length = result.getData(0) * speedPlayer;
        double angle = result.getData(1) * 360;

        Point2D direction = Util.rotateVector(new Point2D(0, 1).multiply(length), angle);
        Point2D pointToWalk = player.add(direction);
        return Math.round(pointToWalk.getX()) + " " + Math.round(pointToWalk.getY());
    }


}
