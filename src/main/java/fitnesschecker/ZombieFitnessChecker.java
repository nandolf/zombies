package fitnesschecker;

import data.DataProvider;
import genetic.genome.Genome;
import genetic.neuronalnetwork.NetworkMakerZombies;
import genetic.neuronalnetwork.NetworkZombies;
import genetic.sensors.BlockSensor;
import genetic.util.SensorMixer;
import simulator.Simulator;
import strategy.NeuronStrategy;

import java.util.ArrayList;
import java.util.List;

public class ZombieFitnessChecker implements FitnessChecker {
    private List<BlockSensor> playerSensors = new ArrayList<>();
    private List<BlockSensor> humanSensors = new ArrayList<>();
    private List<BlockSensor> zombieSensors = new ArrayList<>();

    public ZombieFitnessChecker(List<BlockSensor> playerSensors, List<BlockSensor> humanSensors, List<BlockSensor> zombieSensors) {
        this.playerSensors = playerSensors;
        this.humanSensors = humanSensors;
        this.zombieSensors = zombieSensors;
    }

    @Override
    public long score(Genome genome) {
        NetworkZombies network = NetworkMakerZombies.generateNetwork(genome, SensorMixer.mixSensors(playerSensors, humanSensors, zombieSensors));
        return simulateData(DataProvider.hoard(), network);
    }

    private long simulateData(DataProvider dataProvider, NetworkZombies network) {
        Simulator simulator = new Simulator(dataProvider, (player, humans, zombies, score) -> {
        });

        if (network.hasCycle())
            return -1;
        return simulator.simulate(new NeuronStrategy(network));
    }

}
