package fitnesschecker;


import genetic.genome.Genome;
import genetic.neuronalnetwork.NetworkAnd;
import genetic.neuronalnetwork.NetworkMakerAnd;
import genetic.sensors.PointSensor;

public class AndFitnessChecker implements FitnessChecker {
    private PointSensor sensorA;
    private PointSensor sensorB;

    private NetworkAnd network;

    public AndFitnessChecker(PointSensor sensorA, PointSensor sensorB) {
        this.sensorA = sensorA;
        this.sensorB = sensorB;
    }

    @Override
    public long score(Genome genome) {
        network = NetworkMakerAnd.generateNetwork(genome, sensorA, sensorB);
        if (network.hasCycle())
            return -1L;
        return (long) test00() + test01() + test10() + test11();
    }

    private int test00() {
        return expectResult(false, false, 0);
    }

    private int test01() {
        return expectResult(false, true, 0);
    }

    private int test10() {
        return expectResult(true, false, 0);
    }

    private int test11() {
        return expectResult(true, true, 1);
    }

    private int expectResult(boolean a, boolean b, int expectedResult) {
        network.setA(a);
        network.setB(b);
        if (network.getOuput() == expectedResult) {
            return 1;
        }
        return 0;
    }


}
