package fitnesschecker;

import genetic.genome.Genome;

public interface FitnessChecker {
	long score(Genome genome);
}
