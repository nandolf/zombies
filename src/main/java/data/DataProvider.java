package data;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

public class DataProvider {

    private Point2D player;
    private List<Point2D> humans;
    private List<Point2D> zombies;
    private String levelName;

    public DataProvider(String player, String humansSet, String zombiesSet, String levelName) {
        this.player = parseSingle(player);
        this.humans = parseMultiple(humansSet);
        this.zombies = parseMultiple(zombiesSet);
        this.levelName = levelName;
    }

    private Point2D parseSingle(String figure) {
        String[] koordinates = figure.split(";");
        return new Point2D(Double.parseDouble(koordinates[0]), Double.parseDouble(koordinates[1]));
    }

    private List<Point2D> parseMultiple(String figuresString) {
        List<Point2D> result = new ArrayList<>();
        String[] figures = figuresString.replace(" ", "").replace("[", "").replace("]", "").split(",");
        for (String figure : figures) {
            result.add(parseSingle(figure));
        }
        return result;
    }

    public Point2D getPlayer() {
        return player;
    }

    public List<Point2D> getHumans() {
        return humans;
    }

    public List<Point2D> getZombies() {
        return zombies;
    }

    public String getLevelName() {
        return levelName;
    }

    public static DataProvider simple() {
        return new DataProvider("0;0", "[8250;4500]", "[8250;8999]", "simple");
    }

    public static DataProvider twoZombies() {
        return new DataProvider("5000;0", "[950;6000, 8000;6100]", "[3100;7000, 11500;7100]", "twoZombies");
    }

    public static DataProvider twoZombiesRedux() {
        return new DataProvider("10999;0", "[8000;5500, 4000;5500]", "[1250;5500, 15999;5500]", "twoZombiesRedux");
    }

    public static DataProvider scaredHuman() {
        return new DataProvider("8000;2000", "[8000;4500]", "[2000;6500, 14000;6500]", "scaredHuman");
    }


    public static DataProvider threeVsThree() {
        return new DataProvider("7500;2000", "[9000;1200, 400;6000]", "[2000;1500, 13900;6500, 7000;7500]", "threeVsThree");
    }

    public static DataProvider comboOpportunity() {
        return new DataProvider("500;4500", "[100;4000, 130;5000, 10;4500, 500;3500, 10;5500, 100;3000]", " [8000;4500, 9000;4500, 10000;4500, 11000;4500, 12000;4500, 13000;4500, 14000;4500, 15000;3500, 14500;2500, 15900;500]", "comboOpportunity");
    }

    public static DataProvider rowsToDefend() {
        return new DataProvider("0;4000", "[0;1000, 0;8000]", "[5000;1000, 5000;8000, 7000;1000, 7000;8000, 9000;1000, 9000;8000, 11000;1000, 11000;8000, 13000;1000, 13000;8000, 14000;1000, 14000;8000, 14500;1000, 14500;8000, 15000;1000, 15000;8000]", "rowsToDefend");
    }

    public static DataProvider rowsToDefendRedux() {
        return new DataProvider("0;4000", "[0;1000, 0;8000]", "[3000;1000, 3000;8000, 4000;1000, 4000;8000, 5000;1000, 5000;8000, 7000;1000, 7000;8000, 9000;1000, 9000;8000, 11000;1000, 11000;8000, 13000;1000, 13000;8000, 14000;1000, 14000;8000, 14500;1000, 14500;8000, 15000;1000, 15000;8000]", "rowsToDefendRedux");
    }

    public static DataProvider rectangle() {
        return new DataProvider("8000;4500", "[4000;2250, 4000;6750, 12000;2250, 12000;6750]", "[4000;3375, 12000;3375, 4000;4500, 12000;4500, 4000;5625, 12000;5625, 6000;2250, 8000;2250, 10000;2250, 6000;6750, 8000;6750, 10000;6750]", "rectangle");
    }

    public static DataProvider cross() {
        return new DataProvider("8000;0", "[0;4500, 15999;4500, 8000;7999]", "[2000;1200, 3000;1800, 4000;2400, 5000;3000, 6000;3600, 9000;5400, 10000;6000, 11000;6600, 12000;7200, 13000;7800, 14000;8400, 14000;600, 13000;1200, 12000;1800, 11000;2400, 10000;3000, 9000;3600, 6000;5400, 5000;6000, 4000;6600, 3000;7200, 2000;7800, 1000;8400]", "cross");
    }

    public static DataProvider unavoidableDeaths() {
        return new DataProvider("9000;684", "[15999;4500, 8000;7999, 0;4500]", "[0;3033, 1500;6251, 3000;2502, 4500;6556, 6000;3905, 7500;5472, 10500;2192, 12000;6568, 13500;7448]", "unavoidableDeaths");
    }

    public static DataProvider columnsOfDeath() {
        return new DataProvider("8000;4000", "[0;4000, 15000;4000]", "[4333;1800, 4333;3600, 4333;5400, 4333;7200, 10666;1800, 10666;3600, 10666;5400, 10666;7200, 0;7200]", "columnsOfDeath");
    }

    public static DataProvider rescueMission() {
        return new DataProvider("4920;6810", "[50;4810, 14820;3870, 10869;8250, 9695;7220, 10160;5600, 12988;5820, 14892;5180, 881;1210, 7258;2130, 13029;6990]", "[11048;720, 2155;1650, 9618;2820, 12157;3770, 2250;5180, 8617;4890, 7028;960, 1518;280, 7996;4080, 13029;150, 3119;4600, 3339;4150, 894;7340, 7550;7550]", "rescueMission");
    }

    public static DataProvider triangle() {
        return new DataProvider("8020;3500", "[11000;1000, 11000;6000, 4000;3500]", "[15000;1000, 15000;6000, 120;3500, 0;4000, 120;3000]", "triangle");
    }

    public static DataProvider graveDanger() {
        return new DataProvider("3900;5000", "[3000;3000, 3000;5000, 3000;7000, 12000;3500]", "[10000;1000, 10000;6000, 15500;2000, 15500;3600, 15500;5000, 0;1200]", "graveDanger");
    }

    public static DataProvider grid() {
        return new DataProvider("3989;3259", "[302;6109, 3671;981, 6863;809]", "[208;156, 10129;711, 13229;413, 203;3627, 7310;3912, 9814;3223, 13556;3668, 3923;6251, 6720;6574, 10387;6136, 13093;6253]", "grid");
    }

    public static DataProvider hoard() {
        return new DataProvider("3989;3259", "[3647;384, 60;3262, 2391;1601, 2363;3422]", "[6485;499, 7822;446, 9202;826, 11060;253, 12568;808, 14148;650, 6571;1893, 8484;2013, 9669;1968, 7570;3338, 9780;3611, 8360;4767, 9804;4154, 10935;4977, 12310;4614, 13891;4302, 913;5636, 2410;5912, 3952;6143, 4615;5995, 6568;6085, 8204;5579, 9049;5470, 30;6798, 1798;6682, 3247;7664, 5005;7319, 6415;7094, 8159;7447, 9550;6847]", "hoard");
    }

    public static DataProvider flanked() {
        return new DataProvider("3989;3259", "[647;384, 60;1262, 1391;1601, 1363;422, 15470;384, 15060;1262, 11391;1601, 11363;422]", "[7900;1579, 8500;2470, 7500;3798, 6500;4682, 9000;5664, 7500;6319, 8500;7094, 7800;8447, 8100;8847, 0;7000, 1000;7900, 3000;8500, 5000;7500, 7000;6500, 9000;7000, 11000;7500, 13000;8500, 15000;7800]", "flanked");
    }

    public static DataProvider splitSecondReflex() {
        return new DataProvider("8000;4500", "[3000;4500, 14000;4500]", "[2500;4500, 15500;6500]", "splitSecondReflex");
    }

    public static DataProvider swervyPattern() {
        return new DataProvider("0;4500", "[7000;3500, 0;500, 7000;5500, 3500;1000, 9250;8000, 13000;4500]", "[3600;3500, 3700;4500, 3400;6500, 9000;3500, 8990;4500, 9000;5500, 11000;4000, 9100;10]", "swervyPattern");
    }

    public static DataProvider devasation() {
        return new DataProvider("7992;8304", "[757;3545, 510;8170, 1119;733, 1416;7409, 1110;8488, 2118;1983, 3167;480, 6576;664, 8704;1276, 13340;5663, 13808;4731, 15355;3528, 15495;5035, 15182;6184, 15564;7640]", "[3996;4152, 3996;4844, 3996;7612, 5328;1384, 7992;3460, 11322;5536, 11322;8304]", "devasation");
    }

    public static List<DataProvider> getAllSzenarios() {
        return Lists.newArrayList(
                simple(),
                twoZombies(),
                twoZombiesRedux(),
                scaredHuman(),
                threeVsThree(),
                comboOpportunity(),
                rowsToDefend(),
                rowsToDefendRedux(),
                rectangle(),
                cross(),
                unavoidableDeaths(),
                columnsOfDeath(),
                rescueMission(),
                triangle(),
                graveDanger(),
                grid(),
                hoard(),
                flanked(),
                splitSecondReflex(),
                swervyPattern(),
                devasation());
    }

    @Override
    public String toString() {
        return "DataProvider [player=" + player + ", humans=" + humans + ", zombies=" + zombies + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataProvider that = (DataProvider) o;

        if (player != null ? !player.equals(that.player) : that.player != null) return false;
        if (humans != null ? !humans.equals(that.humans) : that.humans != null) return false;
        return zombies != null ? zombies.equals(that.zombies) : that.zombies == null;
    }

    @Override
    public int hashCode() {
        int result = player != null ? player.hashCode() : 0;
        result = 31 * result + (humans != null ? humans.hashCode() : 0);
        result = 31 * result + (zombies != null ? zombies.hashCode() : 0);
        return result;
    }
}
