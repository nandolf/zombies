package genetic.genome;

public enum Type {
    SENSOR, HIDDEN, OUTPUT
}