package genetic.genome;

import com.google.common.collect.Sets;
import genetic.RandomConstants;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Genome {
    private Set<ConnectionGene> connectionGenes = new HashSet<>();
    private Set<NodeGene> nodeGenes = new TreeSet<>();
    private long fitness = 0;
    private int numberOfSensors = 0;
    private int numberOfOutputs = 0;

    private Random random = new Random(System.currentTimeMillis());
    private InnovationNumberGenerator innovationNumberGenerator = InnovationNumberGenerator.getInstance();

    public Genome(int numberOfSensors, int numbersOfOutputs) {
        this.numberOfSensors = numberOfSensors;
        this.numberOfOutputs = numbersOfOutputs;
        addBiasSensor();
        addNodeGenesForSensors();
        addNodeGenesForOutputNeuron();
    }

    private void addBiasSensor() {
        nodeGenes.add(new NodeGene(Type.SENSOR, nodeGenes.size() + 1));
    }

    private void addNodeGenesForSensors() {
        for (int i = 0; i < numberOfSensors; i++)
            nodeGenes.add(new NodeGene(Type.SENSOR, nodeGenes.size() + 1));
    }

    private void addNodeGenesForOutputNeuron() {
        for (int i = 0; i < numberOfOutputs; i++)
            nodeGenes.add(new NodeGene(Type.OUTPUT, nodeGenes.size() + 1));
    }

    public Set<ConnectionGene> getConnectionGenes() {
        return connectionGenes;
    }

    public Set<NodeGene> getNodeGenes() {
        return nodeGenes;
    }

    public void addRandomConnection() {
        int randomIn;
        int randomOut;
        do {
            randomIn = getRandomNode().number;
            randomOut = getRandomNode().number;
        } while (connectionAlreadyExist(randomIn, randomOut) || isSensor(randomOut));
        double randomWeight = random.nextDouble() * 10 * (random.nextBoolean() ? -1 : 1);
        boolean randomEnabled = random.nextBoolean();
        connectionGenes.add(new ConnectionGene(randomIn, randomOut, randomWeight, randomEnabled,
                innovationNumberGenerator.getNextInnovationNumber()));
    }

    private boolean isSensor(int nodeNumber) {
        for (NodeGene node : nodeGenes)
            if (node.number == nodeNumber)
                if (node.type.equals(Type.SENSOR))
                    return true;
        return false;
    }

    private NodeGene getRandomNode() {
        int randomElementIndex = random.nextInt(nodeGenes.size() - 1) + 1;
        int i = 0;
        for (NodeGene node : nodeGenes) {
            if (i == randomElementIndex)
                return node;
            i++;
        }
        throw new IllegalStateException("randomIndex bigger than set");
    }

    private boolean connectionAlreadyExist(int in, int out) {
        if (connectionGenes.isEmpty())
            return false;
        return connectionGenes.stream().filter(connection -> connection.in == in && connection.out == out).count() > 0;
    }

    public void addNodeOnRandomConnection() {
        if (connectionGenes.isEmpty())
            addRandomConnection();
        NodeGene newNode = new NodeGene(Type.HIDDEN, nodeGenes.size() + 1);
        nodeGenes.add(newNode);
        ConnectionGene randomConnection = getRandomConnection();
        randomConnection.enabled = false;

        ConnectionGene e = new ConnectionGene(randomConnection.in, newNode.number, 1.0, true,
                innovationNumberGenerator.getNextInnovationNumber());
        ConnectionGene e2 = new ConnectionGene(newNode.number, randomConnection.out, randomConnection.weight, true,
                innovationNumberGenerator.getNextInnovationNumber());
        connectionGenes.add(e);
        connectionGenes.add(e2);
    }

    public void mutateConnection() {
        if (connectionGenes.isEmpty()) {
            addRandomConnection();
            return;
        }
        ConnectionGene randomConnection = getRandomConnection();
        if (RandomConstants.eventTrue(RandomConstants.newRandomWeight)) {
            double randomWeight = random.nextDouble() * 10 * (random.nextBoolean() ? -1 : 1);
            randomConnection.weight = randomWeight;
        } else {
            double randomWeight = random.nextDouble() * (random.nextBoolean() ? -1 : 1);
            randomConnection.weight += randomWeight;
        }
    }

    private ConnectionGene getRandomConnection() {
        ArrayList<ConnectionGene> connectionList = new ArrayList<>(connectionGenes);
        return connectionList.get(random.nextInt(connectionList.size()));
    }

    public Genome crossover(Genome other) {
        Genome genome = new Genome(numberOfSensors, numberOfOutputs);

        // innovation numbers miteinander kombinieren
        for (ConnectionGene con : getSameConnections(other)) {
            ConnectionGene connectionGeneThis = getConnectionGene(this, con.innovationNumber);
            ConnectionGene connectionGeneOther = getConnectionGene(other, con.innovationNumber);
            ConnectionGene randomGene = random.nextBoolean() ? connectionGeneOther : connectionGeneThis;
            genome.connectionGenes.add(disableRule(connectionGeneThis, connectionGeneOther, randomGene));
        }

        Set<ConnectionGene> disjointAndExcessConnections = Sets.union(disjointConnections(other), excessedConnections(
                other));
        // disjoint oder excess von fitterem übernehmen wenn gleich => zufall
        if (fitness == other.fitness) {
            for (ConnectionGene con : disjointAndExcessConnections) {
                Genome takeFrom = random.nextBoolean() ? this : other;
                if (containsInnovationNumber(takeFrom.connectionGenes, con)) {
                    genome.connectionGenes.add(disableRule(con));
                }
            }
        } else {
            Genome fittest = getFittest(this, other);
            intersectionInnoNumber(fittest.connectionGenes, disjointAndExcessConnections).forEach(
                    con -> genome.connectionGenes.add(disableRule(con)));
        }

        genome.nodeGenes.addAll(usedHiddenNodes(other, genome));
        return genome;
    }

    private static boolean containsInnovationNumber(Set<ConnectionGene> set, ConnectionGene connection) {
        for (ConnectionGene con : set) {
            if (con.innovationNumber == connection.innovationNumber)
                return true;
        }
        return false;
    }

    private static Set<ConnectionGene> intersectionInnoNumber(Set<ConnectionGene> connectionSetA,
                                                              Set<ConnectionGene> connectionSetB) {
        return connectionSetA.stream().filter(con -> containsInnovationNumber(connectionSetB, con)).collect(Collectors
                .toSet());
    }

    private List<NodeGene> usedHiddenNodes(Genome other, Genome genome) {
        Set<NodeGene> nodesToAdd = new HashSet<>(nodeGenes);
        nodesToAdd.addAll(other.nodeGenes);
        Set<Integer> nonEmptyNodes = new HashSet<>();
        for (ConnectionGene con : genome.connectionGenes) {
            nonEmptyNodes.add(con.in);
            nonEmptyNodes.add(con.out);
        }

        return nodesToAdd.stream()
                .filter(node -> node.type.equals(Type.HIDDEN))
                .filter(node -> nonEmptyNodes.contains(node.number))
                .collect(Collectors.toList());
    }

    private ConnectionGene disableRule(ConnectionGene connection) {
        ConnectionGene connectionResult = connection.clone();
        if (!connection.enabled) {
            if (RandomConstants.eventTrue(RandomConstants.disableIfEitherDisabled))
                connectionResult.enabled = false;
            else
                connectionResult.enabled = true;
        }
        return connectionResult;
    }

    private ConnectionGene disableRule(ConnectionGene a, ConnectionGene b, ConnectionGene result) {
        ConnectionGene connectionResult = result.clone();
        if (!a.enabled ^ !b.enabled) {
            if (RandomConstants.eventTrue(RandomConstants.disableIfEitherDisabled))
                connectionResult.enabled = false;
            else
                connectionResult.enabled = true;
        }
        return connectionResult;
    }

    private Set<ConnectionGene> excessedConnections(Genome other) {
        long minMax = Math.min(maxInnovationNumber(connectionGenes), maxInnovationNumber(other.connectionGenes));
        return Stream.concat(connectionGenes.stream(), other.connectionGenes.stream())
                .filter(connection -> connection.innovationNumber > minMax).collect(Collectors.toSet());
    }

    private Set<ConnectionGene> disjointConnections(Genome other) {
        return Stream.concat(connectionGenes.stream(), other.connectionGenes.stream())
                .filter(connection -> !containsInnovationNumber(other.connectionGenes, connection)
                        || !containsInnovationNumber(connectionGenes, connection))
                .filter(con -> !containsInnovationNumber(excessedConnections(other), con)).collect(Collectors.toSet());
    }

    private static Genome getFittest(Genome a, Genome b) {
        if (b.fitness > a.fitness) {
            return b;
        }
        return a;
    }

    private Set<ConnectionGene> getSameConnections(Genome other) {
        Set<Long> otherInnovationNumbers = other.connectionGenes.stream().map(con -> con.innovationNumber).collect(
                Collectors.toSet());
        return connectionGenes.stream().filter(con -> otherInnovationNumbers.contains(con.innovationNumber)).collect(
                Collectors.toSet());
    }

    public double geneticDistance(Genome other) {
        int excessed = excessedConnections(other).size();
        int disjoint = disjointConnections(other).size();
        double weightDifference = averageWeightDifference(other);
        double n = normalizeFactor(other);
        return (1 * excessed / n) + (1 * disjoint / n) + 0.4 * weightDifference;
    }

    private double normalizeFactor(Genome other) {
        if (Math.min(connectionGenes.size(), other.connectionGenes.size()) < 20)
            return 1;
        return (double) Math.max(connectionGenes.size(), other.connectionGenes.size())
                / Math.min(connectionGenes.size(), other.connectionGenes.size());
    }

    private static long maxInnovationNumber(Collection<ConnectionGene> connections) {
        return connections.stream().mapToLong(connection -> connection.innovationNumber).max().orElse(0);
    }

    private double averageWeightDifference(Genome other) {
        Set<ConnectionGene> intersection = intersectionInnoNumber(connectionGenes, other.connectionGenes);

        double weightDifference = 0;
        for (ConnectionGene connection : intersection) {
            weightDifference += Math
                    .abs(connection.weight - getConnectionGene(other, connection.innovationNumber).weight);
        }
        weightDifference /= Math.max(1, intersection.size());
        return weightDifference;
    }

    private ConnectionGene getConnectionGene(Genome other, long innovationNumber) {
        for (ConnectionGene con : other.connectionGenes) {
            if (con.innovationNumber == innovationNumber)
                return con;
        }
        throw new IllegalStateException("connection not found");
    }

    @Override
    public String toString() {
        return "genome [connectionGenes=" + connectionGenes + ", nodeGenes=" + nodeGenes + ", fitness=" + fitness + "]";
    }

    public long getFitness() {
        return fitness;
    }

    public void setFitness(long fitness) {
        this.fitness = fitness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Genome genome = (Genome) o;

        if (fitness != genome.fitness)
            return false;
        if (connectionGenes != null ? !connectionGenes.equals(genome.connectionGenes) : genome.connectionGenes != null)
            return false;
        return nodeGenes != null ? nodeGenes.equals(genome.nodeGenes) : genome.nodeGenes == null;
    }

    @Override
    public int hashCode() {
        int result = connectionGenes != null ? connectionGenes.hashCode() : 0;
        result = 31 * result + (nodeGenes != null ? nodeGenes.hashCode() : 0);
        result = 31 * result + (int) (fitness ^ (fitness >>> 32));
        return result;
    }

    @Override
    public Genome clone() {
        Genome genome = new Genome(numberOfSensors, numberOfOutputs);
        for (ConnectionGene con : connectionGenes)
            genome.connectionGenes.add(con.clone());
        for (NodeGene node : nodeGenes)
            genome.nodeGenes.add(node.clone());
        genome.setFitness(fitness);
        return genome;
    }
}
