package genetic.genome;

import java.util.concurrent.atomic.AtomicLong;

public class InnovationNumberGenerator {
	private AtomicLong innovationNumber = new AtomicLong(1);

	private static final InnovationNumberGenerator instance = new InnovationNumberGenerator();

	public long getNextInnovationNumber() {
		return innovationNumber.getAndIncrement();
	}

	private InnovationNumberGenerator() {
	}

	// Runtime initialization
	// By defualt ThreadSafe
	public static InnovationNumberGenerator getInstance() {
		return instance;
	}
}
