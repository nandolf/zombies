package genetic.genome;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
public class NodeGene implements Comparable<NodeGene> {
    @Getter
    Type type;
    @Getter
    int number;

    public NodeGene(Type type, int number) {
        this.type = type;
        this.number = number;
    }

    @Override
    public NodeGene clone() {
        return new NodeGene(type, number);
    }

    @Override
    public String toString() {
        return "NodeGene [type=" + type + ", number=" + number + "]";
    }

    @Override
    public int compareTo(NodeGene other) {
        return Integer.compare(number, other.number);
    }
}
