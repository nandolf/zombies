package genetic.genome;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
@Getter
public class ConnectionGene {

    public ConnectionGene(int in, int out, double weight, boolean enabled, long innovationNumber) {
        this.in = in;
        this.out = out;
        this.weight = weight;
        this.enabled = enabled;
        this.innovationNumber = innovationNumber;
    }

    int in;
    int out;
    double weight;
    boolean enabled;
    long innovationNumber;

    @Override
    public ConnectionGene clone() {
        return new ConnectionGene(in, out, weight, enabled, innovationNumber);
    }

    @Override
    public String toString() {
        return "ConnectionGene [in=" + in + ", out=" + out + ", weight=" + weight + ", enabled=" + enabled
                + ", innovationNumber=" + innovationNumber + "]";
    }
}
