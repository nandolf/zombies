package genetic;

import java.util.Random;

public class RandomConstants {

	private static Random random = new Random();

	public static final double percentageGenerationMutation = 0.25;

	public static final double connectionMutation = 0.8;
	public static final double newRandomWeight = 0.1;

	public static final double disableIfEitherDisabled = 0.75;
	public static final double mutationInsteadOfCrossover = 0.25;

	public static final double interspeciesCrossover = 0.001;

	public static final double addingNodePopulation = 0.03;
	public static final double addingLinkPopulation = 0.05;

	public static boolean eventTrue(double percent) {
		if (random.nextDouble() <= percent)
			return true;
		return false;
	}
}
