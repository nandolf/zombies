package genetic.util;

import genetic.sensors.Sensor;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class SensorMixer {

    public static <T extends Sensor> List<T> mixSensors(List<T> playerSensors, List<T> humanSensors,
                                                        List<T> zombieSensors) {
        if (playerSensors.size() != humanSensors.size() || humanSensors.size() != zombieSensors.size())
            throw new IllegalStateException("All sensors must have the same Size.: " + playerSensors.size() + " "
                    + humanSensors.size() + " " + zombieSensors.size());
        Deque<T> playerSensorDeque = new ArrayDeque<>(playerSensors);
        Deque<T> humanSensorDeque = new ArrayDeque<>(humanSensors);
        Deque<T> zombieSensorDeque = new ArrayDeque<>(zombieSensors);

        List<T> mixedSensors = new ArrayList<>(playerSensors.size() * 3);
        while (!playerSensorDeque.isEmpty()) {
            mixedSensors.add(playerSensorDeque.pop());
            mixedSensors.add(humanSensorDeque.pop());
            mixedSensors.add(zombieSensorDeque.pop());
        }
        return mixedSensors;
    }

}
