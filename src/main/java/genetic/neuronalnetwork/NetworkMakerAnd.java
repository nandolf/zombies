package genetic.neuronalnetwork;

import com.google.common.collect.Lists;
import genetic.genome.Genome;
import genetic.sensors.PointSensor;

import java.util.List;

public class NetworkMakerAnd {

    public static NetworkAnd generateNetwork(Genome genome, PointSensor sensorA, PointSensor sensorB) {
        List<Neuron> outputs = NetworkMaker.getOutputs(genome, Lists.newArrayList(sensorA, sensorB));
        if (outputs.size() > 1)
            throw new IllegalStateException("too many outputs for NetworkAnd");
        return new NetworkAnd(sensorA, sensorB, outputs.get(0));
    }


}
