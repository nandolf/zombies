package genetic.neuronalnetwork;

import genetic.sensors.PointSensor;

import java.util.Collections;

public class NetworkAnd extends Network {

    private PointSensor sensorA;
    private PointSensor sensorB;
    private Neuron outputNeuron;

    public NetworkAnd(PointSensor sensorA, PointSensor sensorB, Neuron output) {
        super(Collections.singletonList(output));
        this.outputNeuron = output;
        this.sensorA = sensorA;
        this.sensorB = sensorB;
    }

    public int getOuput() {
        return (int) Math.signum(outputNeuron.getOutput());
    }

    public void setA(boolean activate) {
        if (activate)
            sensorA.activate();
        else
            sensorA.deactivate();
    }

    public void setB(boolean activate) {
        if (activate)
            sensorB.activate();
        else
            sensorB.deactivate();
    }

}
