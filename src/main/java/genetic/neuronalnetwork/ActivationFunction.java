package genetic.neuronalnetwork;

@FunctionalInterface
public interface ActivationFunction {
	double output(double sum);
}
