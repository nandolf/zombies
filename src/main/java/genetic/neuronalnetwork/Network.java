package genetic.neuronalnetwork;

import java.util.List;

public class Network {
    protected List<Neuron> outputs;

    public Network(List<Neuron> outputs) {
        this.outputs = outputs;
    }

    public boolean hasCycle() {
        for (Neuron outputNeuron : outputs) {
            if (hasCycle(outputNeuron)) {
                return true;
            }
        }
        return false;
    }

    private boolean hasCycle(Neuron legthOutput2) {
        CycleVisitor visitor = new CycleVisitor();
        legthOutput2.accept(visitor);
        return visitor.cycleDetected();
    }
}
