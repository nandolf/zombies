package genetic.neuronalnetwork;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Neuron implements Input {

	private Map<Input, Double> inputs = new HashMap<>();
	private ActivationFunction activationFunction = defaultFunction;

	private long id;

	public static ActivationFunction defaultFunction = (sum) -> sum > 0 ? 1 : 0;
	public static ActivationFunction sigmoid = (sum) -> 1 / (1 + Math.exp(-4.9 * sum));
	public static ActivationFunction outputFunction = (sum) -> {
		if (sum > 1)
			return 1;
		else if (sum < 0)
			return 0;
		return sum;
	};

    public Neuron(long id) {
        this.id = id;
    }

	public Neuron(long id, ActivationFunction activationFunction) {
		this.id = id;
		this.activationFunction = activationFunction;
	}

	public Neuron(long id, Map<Input, Double> inputs, ActivationFunction activationFunction) {
		this.id = id;
		this.inputs = inputs;
		this.activationFunction = activationFunction;
	}

	Set<Input> getInputs() {
		return inputs.keySet();
	}

	@Override
	public double getOutput() {
		double sum = 0;
		for (Map.Entry<Input, Double> input : inputs.entrySet()) {
			sum += input.getKey().getOutput() * input.getValue();
		}
		return activationFunction.output(sum);
	}

	public void addInput(Input input, double weight) {
		inputs.put(input, weight);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Neuron other = (Neuron) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
    public void accept(CycleVisitor visitor) {
        visitor.visit(this);
    }

	@Override
	public String toString() {
		return "Neuron [id=" + id + "]";
	}

}
