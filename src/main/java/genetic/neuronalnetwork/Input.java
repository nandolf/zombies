package genetic.neuronalnetwork;

public interface Input {
	double getOutput();

    void accept(CycleVisitor visitor);
}
