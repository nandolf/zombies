package genetic.neuronalnetwork;

import genetic.genome.ConnectionGene;
import genetic.genome.Genome;
import genetic.genome.NodeGene;
import genetic.sensors.PointSensor;
import genetic.sensors.Sensor;

import java.util.*;

public class NetworkMaker {

    public static <T extends Sensor> List<Neuron> getOutputs(Genome genome, List<T> orderedSensors) {
        Deque<Sensor> sensorDeque = new ArrayDeque<>(orderedSensors);

        // erstelle neuronen
        PointSensor bias = null;
        Map<Integer, Input> neurons = new HashMap<>();
        List<Neuron> outputs = new LinkedList<>();

        for (NodeGene nodeGene : genome.getNodeGenes()) {
            switch (nodeGene.getType()) {
                case SENSOR:
                    if (bias == null) {
                        bias = new PointSensor();
                        bias.activate();
                        neurons.put(nodeGene.getNumber(), bias);
                    } else {
                        neurons.put(nodeGene.getNumber(), sensorDeque.pop());
                        break;
                    }
                    break;
                case HIDDEN:
                    neurons.put(nodeGene.getNumber(), new Neuron(nodeGene.getNumber()));
                    break;
                case OUTPUT:
                    Neuron output = new Neuron(nodeGene.getNumber());
                    neurons.put(nodeGene.getNumber(), output);
                    outputs.add(output);
                    break;
                default:
                    throw new IllegalStateException("Unknown Type");
            }
        }
        if (!sensorDeque.isEmpty()) {
            System.out.println(sensorDeque);
            throw new IllegalStateException("not all sensors are used");
        }

        if (outputs.isEmpty())
            throw new IllegalArgumentException("OUTPUT UNKNOWN!");

        // f?ge verbindungen hinzu
        for (ConnectionGene connect : genome.getConnectionGenes()) {
            if (connect.isEnabled()) {
                if (!neurons.containsKey(connect.getOut()) || !neurons.containsKey(connect.getIn()))
                    throw new IllegalStateException("Unknown Node: " + connect.getOut() + " or " + connect.getIn());
                if (!(neurons.get(connect.getOut()) instanceof Neuron))
                    throw new IllegalStateException("Connection to Sensor");
                // outs sind immer neuronen, keine sensoren => addInput geht
                ((Neuron) neurons.get(connect.getOut())).addInput(neurons.get(connect.getIn()), connect.getWeight());
            }
        }

        return outputs;
    }
}
