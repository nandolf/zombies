package genetic.neuronalnetwork;

import java.util.HashSet;
import java.util.Set;

public class CycleVisitor {

    private Set<Neuron> visited = new HashSet<>();
    private boolean cycle = false;

    public void visit(Input input) {
        if (input instanceof Neuron) {
            Neuron neuron = (Neuron) input;
            if (visited.contains(neuron)) {
                cycle = true;
                return;
            }
            visited.add(neuron);
            neuron.getInputs().forEach(in -> in.accept(this));
        }
    }

    boolean cycleDetected() {
        return cycle;
    }
}
