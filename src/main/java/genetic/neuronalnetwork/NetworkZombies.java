package genetic.neuronalnetwork;

import data.Point2D;
import genetic.sensors.BlockSensor;

import java.util.*;

public class NetworkZombies extends Network {

    private final List<BlockSensor> sensors;

    public NetworkZombies(List<BlockSensor> orderedSensors, Neuron angleOutputs, Neuron lengthOutput) {
        super(Arrays.asList(angleOutputs, lengthOutput));
        this.sensors = orderedSensors;
    }

    public void setPlayer(Point2D player) {
        getPlayerSensors().forEach(sensor -> sensor.setFigures(Collections.singletonList(player)));
    }

    public void setHumans(List<Point2D> humans) {
        getHumanSensors().forEach(sensor -> sensor.setFigures(humans));
    }

    public void setZombies(List<Point2D> zombies) {
        getZombieSensors().forEach(sensor -> sensor.setFigures(zombies));
    }

    private Collection<BlockSensor> getPlayerSensors() {
        return getEvery3rdSensor(0);
    }

    private Collection<BlockSensor> getHumanSensors() {
        return getEvery3rdSensor(1);
    }

    private Collection<BlockSensor> getZombieSensors() {
        return getEvery3rdSensor(2);
    }

    private Collection<BlockSensor> getEvery3rdSensor(int startElement) {
        Set<BlockSensor> sensors = new HashSet<>();
        for (int i = startElement; i < this.sensors.size(); i += 3) {
            sensors.add(this.sensors.get(i));
        }
        return sensors;
    }

    public double getAngleOutput() {
        return outputs.get(0).getOutput();
    }

    public double getLengthOutput() {
        return outputs.get(1).getOutput();
    }
}
