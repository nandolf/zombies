package genetic.neuronalnetwork;

import genetic.genome.Genome;
import genetic.sensors.BlockSensor;

import java.util.List;

public class NetworkMakerZombies {

    public static NetworkZombies generateNetwork(Genome genome, List<BlockSensor> orderedSensors) {
        return new NetworkZombies(orderedSensors, NetworkMaker.getOutputs(genome, orderedSensors).get(0), NetworkMaker.getOutputs(genome, orderedSensors).get(1));
    }


}
