package genetic.sensors;

import data.Point2D;
import genetic.neuronalnetwork.CycleVisitor;

import java.util.ArrayList;
import java.util.List;

public class BlockSensor implements Sensor {

    private Point2D position;
    private int blockSize;
    private List<Point2D> figures = new ArrayList<>();

    public BlockSensor(BlockSensor other) {
        this.position = other.position;
        this.blockSize = other.blockSize;
    }

    public BlockSensor(Point2D position, int blockSize) {
        this.position = position;
        this.blockSize = blockSize;
    }

    @Override
    public double getOutput() {
        double result = 0;
        for (Point2D figure : figures) {
            if (figure == null) {
                System.out.println(figure);
            }
            if (inBlock(figure))
                result++;
        }
        return result;
//        return figures.stream().filter(figure -> inBlock(figure)).count();
    }

    private boolean inBlock(Point2D figure) {
        if (figure == null) {
            System.out.println(figure);
        }
        return Math.abs(figure.getX() - position.getX()) < blockSize
                && Math.abs(figure.getY() - position.getY()) < blockSize;
    }

    public void setFigures(List<Point2D> figures) {
        for (Point2D figure : figures) {
            if (figure == null) {
                System.out.println(figure);
            }
        }
        this.figures = figures;
    }

    @Override
    public void accept(CycleVisitor visitor) {
        visitor.visit(this);
    }

}
