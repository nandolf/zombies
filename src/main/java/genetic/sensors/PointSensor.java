package genetic.sensors;


import genetic.neuronalnetwork.CycleVisitor;

public class PointSensor implements Sensor {

    private boolean activated;

    @Override
    public double getOutput() {
        return activated ? 1 : 0;
    }

    @Override
    public void accept(CycleVisitor visitor) {
        visitor.visit(this);
    }

    public void activate() {
        this.activated = true;
    }

    public void deactivate() {
        this.activated = false;
    }
}
