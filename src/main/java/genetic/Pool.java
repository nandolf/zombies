package genetic;

import fitnesschecker.FitnessChecker;
import genetic.genome.Genome;
import genetic.neuronalnetwork.Network;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

public abstract class Pool {
    static final int POPULATION = 1000;
    private static final double KILL_PERCENT = 0.25;
    private static final double DISTANCE_TRESHHOLD = 3.0;
    private List<Species> speciesPool = new LinkedList<>();


    private Random random = new Random();

    private FitnessChecker fitnessChecker;

    Pool() {
        int numberOfSensors = generateSensors();
        fitnessChecker = chooseFitnessChecker();

        // minimale population mit keinen hidden layer
        for (int i = 0; i < POPULATION; i++) {
            Genome genome = new Genome(numberOfSensors, getNumberOfOutputs());
            addToSpecies(genome);
        }
    }

    protected abstract FitnessChecker chooseFitnessChecker();

    protected abstract int generateSensors();

    protected abstract int getNumberOfOutputs();

    private void addToSpecies(Genome genome) {
        for (Species species : speciesPool) {
            if (species.getRepresentant().geneticDistance(genome) < DISTANCE_TRESHHOLD) {
                species.addGenome(genome);
                return;
            }
        }
        Species newSpecies = new Species(fitnessChecker);
        newSpecies.addGenome(genome);
        newSpecies.chooseRepresentant();
        speciesPool.add(newSpecies);
    }


    public void newGeneration() {
        killWeakGenomes();
        speciesPool.forEach(Species::chooseRepresentant);

        Set<Genome> children = new HashSet<>();
        children.addAll(randomMutationChilds((int) (POPULATION * RandomConstants.percentageGenerationMutation)));
        // jeder darf einmal reproduzieren
        for (Species species : speciesPool) {
            if (RandomConstants.eventTrue(RandomConstants.interspeciesCrossover))
                children.add(doInterSpeciesCrossover());
            else {
                children.add(species.reproduce());
            }
        }

        // rest reproduce zuf�llige reihenfolge
        Collections.shuffle(speciesPool);
        int i = 0;
        while (children.size() < POPULATION) {
            children.add(speciesPool.get(i).reproduce());
            i++;
            i %= speciesPool.size();
        }

        killChildrenOverPopulation(children);

        // TODO InnovationNUmbers wieder gerade ziehen
        replaceCurrentPopulationWithChildren(children);
    }

    private void replaceCurrentPopulationWithChildren(Set<Genome> children) {
        speciesPool.forEach(Species::killGenoms);
        children.forEach(this::addToSpecies);
        removeEmptySpecies();
    }

    private void removeEmptySpecies() {
        speciesPool.removeIf(species -> species.getGenomes().isEmpty());
    }

    private void killChildrenOverPopulation(Set<Genome> childs) {
        List<Genome> sortedGenomesWeakestFirst = childs.stream().sorted(comparing(Genome::getFitness).reversed())
                .collect(Collectors.toList());
        Iterator<Genome> genomeIterator = sortedGenomesWeakestFirst.iterator();
        while (sortedGenomesWeakestFirst.size() > POPULATION) {
            childs.remove(genomeIterator.next());
        }
    }

    private void killWeakGenomes() {
        List<Genome> sortedGenomes = allGenomes().stream().sorted(comparing(this::getAdjustedFitness).reversed())
                .collect(Collectors.toList());
        Iterator<Genome> genomeIterator = sortedGenomes.iterator();
        for (int i = 0; i < (int) (POPULATION * KILL_PERCENT); i++) {
            Genome genomeToRemove = genomeIterator.next();
            getSpeciesOfGenome(genomeToRemove).getGenomes().remove(genomeToRemove);
        }
        removeEmptySpecies();
    }

    private Species getSpeciesOfGenome(Genome genome) {
        return speciesPool.stream().filter(spec -> spec.getGenomes().contains(genome)).findFirst().orElseThrow(
                () -> new IllegalStateException("genome without species"));
    }

    private Set<Genome> randomMutationChilds(int numberOfChilds) {
        Collections.shuffle(speciesPool);
        Set<Genome> childs = new HashSet<>();
        while (true) {
            for (Species species : speciesPool) {
                if (childs.size() >= numberOfChilds)
                    return childs;
                childs.add(species.mutateRandomGenome());
            }
        }
    }

    private double getAdjustedFitness(Genome genome) {
        Species speciesOfGenome = speciesPool.stream().filter(species -> species.getGenomes().contains(genome))
                .findFirst().orElseThrow(() -> new IllegalStateException("genome has no species"));
        return speciesOfGenome.getAdjustetFitness(genome);
    }

    private List<Genome> allGenomes() {
        List<Genome> genomes = new LinkedList<>();
        for (Species species : speciesPool) {
            genomes.addAll(species.getGenomes());
        }
        return genomes;
    }

    private Genome doInterSpeciesCrossover() {
        Species speciesMother = speciesPool.get(random.nextInt(speciesPool.size()));
        Species speciesFather;
        do {
            speciesFather = speciesPool.get(random.nextInt(speciesPool.size()));
        } while (speciesFather.equals(speciesMother));
        Genome child = speciesMother.getRandomGenome().crossover(speciesFather.getRandomGenome());
        child.setFitness(fitnessChecker.score(child));
        return child;
    }

    public abstract Network getBestPerformingInstance();

    public Genome getBestPerformingGenome() {
        return allGenomes().stream().max(comparing(Genome::getFitness)).orElseThrow(
                () -> new IllegalStateException(
                        "no genome found => no best instance"));
    }

    List<Species> getSpeciesPool() {
        return speciesPool;
    }
}
