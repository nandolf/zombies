package genetic;

import fitnesschecker.AndFitnessChecker;
import fitnesschecker.FitnessChecker;
import genetic.neuronalnetwork.NetworkAnd;
import genetic.neuronalnetwork.NetworkMakerAnd;
import genetic.sensors.PointSensor;

public class PoolAnd extends Pool {

    private PointSensor sensorA;
    private PointSensor sensorB;

    @Override
    protected FitnessChecker chooseFitnessChecker() {
        return new AndFitnessChecker(sensorA, sensorB);
    }

    @Override
    protected int generateSensors() {
        sensorA = new PointSensor();
        sensorB = new PointSensor();
        return 2;
    }

    @Override
    protected int getNumberOfOutputs() {
        return 1;
    }

    @Override
    public NetworkAnd getBestPerformingInstance() {
        return NetworkMakerAnd.generateNetwork(getBestPerformingGenome(), sensorA, sensorB);
    }
}

