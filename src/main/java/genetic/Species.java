package genetic;

import fitnesschecker.FitnessChecker;
import genetic.genome.Genome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Species {
    private List<Genome> genomes = new ArrayList<>();
    private Genome representantFromLastGeneration;
    private FitnessChecker fitnessChecker;

    private Random random = new Random();

    Species(FitnessChecker fitnessChecker) {
        this.fitnessChecker = fitnessChecker;
    }

    Genome getRepresentant() {
        return representantFromLastGeneration;
    }

    void chooseRepresentant() {
        representantFromLastGeneration = getRandomGenome();
    }

    void addGenome(Genome genome) {
        genomes.add(genome);
    }

    double getAdjustetFitness(Genome genome) {
        return genome.getFitness() / (double) genomes.size();
    }

    List<Genome> getGenomes() {
        return genomes;
    }

    Genome reproduce() {
        if (RandomConstants.eventTrue(RandomConstants.mutationInsteadOfCrossover)) {
            return mutateRandomGenome();
        } else {
            return speciesCrossover();
        }
    }

    private Genome speciesCrossover() {
        if (genomes.size() == 1) {
            // no crossover possible. return mutated species
            return mutateRandomGenome();
        }
        Genome mother = getRandomGenome();
        Genome father;
        do {
            father = getRandomGenome();
        } while (father == mother);

        Genome child = mother.crossover(father);
        child.setFitness(fitnessChecker.score(child));
        return child;
    }

    Genome mutateRandomGenome() {
        Genome randomGenome = getRandomGenome();
        Genome newGenome;
        do {
            newGenome = randomGenome.clone();
            if (RandomConstants.eventTrue(RandomConstants.addingNodePopulation))
                newGenome.addNodeOnRandomConnection();
            if (RandomConstants.eventTrue(RandomConstants.addingLinkPopulation))
                newGenome.addRandomConnection();
            else {
                newGenome.mutateConnection();
            }
            newGenome.setFitness(fitnessChecker.score(newGenome));
        } while (newGenome.getFitness() < randomGenome.getFitness());
        return newGenome;
    }

    Genome getRandomGenome() {
        if (genomes.isEmpty())
            throw new IllegalStateException("no genomes in species");
        return genomes.get(random.nextInt(genomes.size()));
    }

    public void killGenoms() {
        genomes.clear();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((genomes == null) ? 0 : genomes.hashCode());
        result = prime * result + ((representantFromLastGeneration == null) ? 0
                : representantFromLastGeneration.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Species other = (Species) obj;
        if (genomes == null) {
            if (other.genomes != null)
                return false;
        } else if (!genomes.equals(other.genomes))
            return false;
        if (representantFromLastGeneration == null) {
            if (other.representantFromLastGeneration != null)
                return false;
        } else if (!representantFromLastGeneration.equals(other.representantFromLastGeneration))
            return false;
        return true;
    }
}
