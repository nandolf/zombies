package genetic;

import data.Point2D;
import fitnesschecker.FitnessChecker;
import fitnesschecker.ZombieFitnessChecker;
import genetic.neuronalnetwork.NetworkMakerZombies;
import genetic.neuronalnetwork.NetworkZombies;
import genetic.sensors.BlockSensor;
import genetic.util.SensorMixer;

import java.util.ArrayList;
import java.util.List;

public class PoolZombies extends Pool {

    private static final int BLOCKSIZE = 1000;

    private List<BlockSensor> playerSensors;
    private List<BlockSensor> humanSensors;
    private List<BlockSensor> zombieSensors;

    @Override
    protected int generateSensors() {
        playerSensors = new ArrayList<>();
        humanSensors = new ArrayList<>();
        zombieSensors = new ArrayList<>();

        for (int y = 0; y < 16000 / BLOCKSIZE; y++) {
            for (int x = 0; x < 9000 / BLOCKSIZE; x++) {
                playerSensors.add(new BlockSensor(new Point2D(x, y), BLOCKSIZE));
                humanSensors.add(new BlockSensor(new Point2D(x, y), BLOCKSIZE));
                zombieSensors.add(new BlockSensor(new Point2D(x, y), BLOCKSIZE));
            }
        }
        return playerSensors.size() + humanSensors.size() + zombieSensors.size();
    }

    @Override
    protected int getNumberOfOutputs() {
        return 2;
    }

    @Override
    protected FitnessChecker chooseFitnessChecker() {
        return new ZombieFitnessChecker(playerSensors, humanSensors, zombieSensors);
    }

    @Override
    public NetworkZombies getBestPerformingInstance() {
        return NetworkMakerZombies.generateNetwork(getBestPerformingGenome(), SensorMixer.mixSensors(playerSensors, humanSensors,
                zombieSensors));
    }
}
